#!/bin/bash
#
# lint.sh - verifica la correttezza dei contenuti del sito
#
# Da lanciare prima di un commit.
#

set -eu

tmp_dir=$(mktemp -d)
trap "rm -fr '${tmp_dir}'" EXIT

errors=0
for module in ./scripts/lint/*.sh ; do
    module_name=$(basename ${module} .sh)
    output="${tmp_dir}/${module_name}.out"
    $SHELL $module >${output} 2>&1
    if [ $(stat -c %s ${output}) -gt 0 ]; then
        errors=1
        sed -e "s/^/${module_name}: /" < ${output} >&2
    fi
done

if [ $errors -gt 0 ]; then
  exit 1
fi
exit 0

