#!/bin/sh

msg() {
    echo " [*] $*" >&2
}

die() {
    echo >&2
    echo "ERROR: $*" >&2
    exit 1
}

find_installed() {
    which "$1"
}

is_installed() {
    test -n "$(find_installed "$1")"
}

# We can use the installed version of some tools, or invoke 'go get'
# to build them locally (if Go is installed).
find_or_install() {
    local bin="$1"
    local url="$2"
    local relpath="${3:-}"
    if is_installed "${bin}" ; then
        echo "${bin}"
    else
        if [ ! -x "${GOPATH}/bin/${bin}" ]; then
            echo "Installing ${url}" >&2
            # Go needs to be installed.
            is_installed go || die "Go needs to be installed. See https://golang.org/dl/"
            go get -u -d ${url} >&2 || true
            go install ${url}/${relpath} \
                || die "Could not install ${bin}"
        fi
        echo "${GOPATH}/bin/${bin}"
    fi
}

# Merge a set of JSON dictionaries emitted by scripts in the data.d
# subdirectory into a single JSON dictionary. Keys in the output
# dictionary are the names of the scripts themselves.
generate_json_data() {
    local data_dir="$1"
    local count=0
    echo -n "{"
    for script in "${data_dir}"/* ; do
        # Filter out names with dots, like run-parts.
        case "${script##*/}" in
            *.*) continue ;;
        esac
        if [ ${count} -gt 0 ]; then
            echo -n ","
        fi
        count=$(expr ${count} + 1)
        printf '\n  "%s": %s' "$(basename "${script}")" "$(${script})"
    done
    echo "}"
}

update_website() {
    local root_dir="$1"
    local site_dir="${root_dir}/public"
    local index_dir="${root_dir}/index"

    msg "checking prerequisites"

    # In case we need to build some Go packages locally, set GOPATH to
    # point at the 'build' subdirectory of root_dir.
    build_dir="${root_dir}/build"
    mkdir -p "${build_dir}"
    export GOPATH="${build_dir}"
    export PATH="$PATH:$GOPATH/bin"

    # Install the required Go utilities.
    gostatic=$(find_or_install gostatic github.com/piranha/gostatic)
    sitesearch=$(find_or_install sitesearch git.autistici.org/ai/webtools cmd/...)

    test -d "${site_dir}" || mkdir -p "${site_dir}"

    # Update the pages with dynamic content.
    msg "generating site metadata"
    generate_json_data "${root_dir}/data.d" > "${root_dir}/data.json"

    # Run gostatic. The site map needs to be regenerated every time...
    msg "rendering website"
    rm -f "${site_dir}/all_pages.json" 2>/dev/null || true
    (cd "${root_dir}" && "${gostatic}" config)

    # Fix the site directory.
    rsync -a "${root_dir}/static/" "${site_dir}/static/"
    # Uncomment this to just use a link:
    #if [ ! -e "${site_dir}/static" ]; then
    #    ln -s "../static" "${site_dir}/static"
    #fi

    # Copy the historical static content.
    rsync -a "${root_dir}/ai-historical/" "${site_dir}/ai/"
    
    # Index the site.
    msg "updating search index"
    if [ -e "${index_dir}" ]; then
        rm -fr "${index_dir}"
    fi
    ${sitesearch} --update --index="${index_dir}" --dir="${root_dir}"
}

usage() {
    cat >&2 <<EOF
Usage: update.sh [<OPTIONS>]

Renders the A/I website within its source directory.

Options:

   -s, --source PATH   Source directory

EOF
    exit 1
}

# Provide sane defaults for a local test run.
script_dir=$(dirname "$0")
script_dir=${script_dir:-.}
root_dir=$(cd "${script_dir}/.." && pwd)

while [ $# -gt 0 ] ; do
    case "$1" in
        -s|--source)
            root_dir=$2
            shift
            ;;
        --source=*)
            root_dir="${1##--source=}"
            ;;
        *)
            usage
            ;;
    esac
    shift
done

# Exit on errors.
set -e

update_website "${root_dir}"

exit 0
