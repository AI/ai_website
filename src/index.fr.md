title: Bienvenue sur Autistici/Inventati
----

Bienvenue sur Autistici/Inventati
=================================

A/I est né il y a plus de 10 ans, quand des individus et des
collectifs travaillant autour de la technologie, de l'anonymat, des
cyber-droits et de l'activisme politique se sont rencontrés. Notre but
principal est de fournir à une large base des outils de communication
libres et gratuits, et ainsi de proposer une alternative aux modes de
communication commerciaux. Nous voudrions amener à la conscience des
gens le besoin de protéger leur vie privée et d'échapper au pillage
des données perpétré par les gouvernements et les entreprises.

Nous fournissons plusieurs services gratuits, libres et respectueux de
la vie privée: [email](/services/mail), [blog](/services/blog),
[mailing-listes](/services/lists), [instant messaging](/services/chat) et [autres](/services).

Tous les services sont soumis à approbation, sur la base du respect de
nos [conditions d'utilisateur](/who/policy) et de notre
[manifeste](/who/manifesto)

**[Demande un service](get_service)**

**Suivez [notre blog](https://cavallette.noblogs.org)** (en italien et
quelquefois en anglais).

