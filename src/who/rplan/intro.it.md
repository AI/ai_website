title: Il Piano R* - Introduzione
----

Il Piano R\*: un network di comunicazione resistente
=============================================================================

L'Altra Faccia della Spirale
----------------------------

Nuove forme di espressione e organizzazione si sviluppano, disordinate, per creare relazioni, per produrre, per immaginare. Senza nessun
piano preciso, ma con tante idee condivise, ogni giorno, qualche volta in prima pagina, nascono nuovi modi per vivere in questo camaleontico
mondo, modi di vivere che a volte multinazionali e fondazioni miliardarie tentano di sfruttare per i loro profitti e le loro quotazioni in
borsa.

Tentativi goffi di colmare il "pericoloso vuoto normativo" con leggi dettate dalla battaglia di civiltà, dal terrorismo internazionale,
dalle necessità di sapere tutto di tutti, dall'umidità e dalle cavallette hanno creato un quadro in cui la legislazione, e la sua
applicazione, è solo un "punto di vista", adattabile alle esigenze, alle simpatie e alle prime pagine dei tabloid più alla moda.

Nel dubbio, le istituzioni cercano di chiudere il recinto, in maniera confusa, contraddittoria e frettolosa. Lavorano alacremente per
creare una mentalità chiusa, del terrore; costruendo i pericoli che servono a scongiurare un uso "improprio" della rete, cercando di
realizzare il centro commerciale globale, sicuro e ben illuminato.

Si procede reprimendo, lenti ma non troppo, tutti coloro che mettono in discussione questa nuova, ma non troppo, mentalità, abilmente
sponsorizzata.

Questo, ma non solo questo, ha determinato negli ultimi anni, non solo nei cosiddetti paesi canaglia, ma anche nei sedicenti paesi
democratici (sic!), la violazione più o meno palese di quanto si usa definire diritti civili, libertà di espressione e di parola: la
sistematica intercettazione dei canali di comunicazione, la moltiplicazione di sequestri di siti e caselle di posta, i tentativi più o meno
riusciti di censura dell'informazione scomoda e indipendente, la criminalizzazione dello scambio, della condivisione.

L'immagine commerciale di regno delle meraviglie, Internet, si trasforma, secondo le necessità del momento, in un inferno di pedofili,
truffatori e terroristi.

Calati, nostro malgrado, in questo scenario e partendo da quanto abbiamo vissuto finora, abbiamo provato a immaginare un sistema diverso
per organizzare i nostri servizi, per ribadire la nostra volontà di R\*Esistere.

Abbiamo allora concepito il Piano R\* \[[#1](#1)\]: se volete saperne di più, non vi resta che navigare questa
sezione del sito

<a name="1"></a>**\[1\]** Viste le grandi soddisfazioni che il piano R ha dato al signor gran venerabile Licio Gelli, visto l'enorme successo ottenuto in
questi anni, abbiamo pensato di imparare dai vincitori: quello che ci mancava era una strategia. Abbiamo quindi fatto quello che insegnano a
scuola, abbiamo guardato alla storia del nostro grande paese per trarne ispirazione. Abbiamo letto di tutto: dal Libro Cuore fino al De
prima Repubblica di Giulio Andreotti. Ma quale migliore strategia di quella che si realizza punto per punto a 30 anni dal suo concepimento?
Altro che piano quinquennale. Serviva anche a noi un piano R, anzi di più un piano R\*.
