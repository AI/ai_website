title: Il Piano R*
----

Il Piano R\*
=============

![](/static/img/pianor_rete.png)

**Il Piano R\*** è un progetto per un network di comunicazione resistente.

Il [Collettivo A/I](/about) è lieto di presentarvi il **Piano R\***.

Il **Piano R\*** è uno strumento per sviluppare un network di comunicazione resistente

Resistente perché pensato per sventare quanto più possibile (ma senza deliri di onnipotenza) il rischio che la comunicazione elettronica
offerta dalle nostre strutture indipendenti e autogestite si interrompa.

Ma resistente anche perché legato a un bel sogno, il sogno che il conflitto sia un insieme di pratiche ancora vive che necessitano di
strumenti di comunicazione per diffondersi e prosperare.

Per questo non ci stancheremo mai di ripetere che il modo più sicuro per salvaguardare la privacy delle proprie comunicazioni è l'utilizzo
di strumenti crittografici, unito ad una buona dose di buon senso e di comprensione dei mezzi che si stanno utilizzando.

Se volete saperne di più, potete partire dal nostro [manuale per la difesa personale della privacy](/docs/mail/privacymail):
diffondendo abitudini di comunicazione sicura, contribuirete a difendere la privacy di ognun\* come presupposto
importante di una piu' ampia **critica al controllo**, e farete delle nostre reti di comunicazione un utile strumento per la sopravvivenza
delle vostre/nostre lotte.

In queste pagine:

- vorremmo spiegarvi come è stato pensato e sviluppato il Piano R\*
- vorremmo spiegarvi che cosa ci ha portato a sviluppare il Piano R\* come è ora e che cosa ci ha convinto del
    fatto che fosse piuttosto urgente

Chiunque può contribuire al progetto: se volete suggerirci qualcosa o inviarci considerazioni e critiche, scrivete a <info@autistici.org>
(usando la chiave GPG che troverete [qui](/get_help#gpgkey "A/I GPG key") sarebbe la cosa ideale)

Cominciamo il nostro tour

- [Un'introduzione al Piano R\*](/who/rplan/intro "un'introduzione al Piano R*")
- [Come funziona il Piano R\*](/who/rplan/how "Come funziona il Piano R*")
- [Cosa significa in pratica il Piano R\*](/who/rplan/what "Cosa significa in pratica il Piano R*")
- [Diapositive riassuntive del Piano R\*](http://www.autistici.org/orangebook/slides/orangebook.en.html "Diapositive sul Piano R*")

Il Collettivo A/I

Ottobre 2005


