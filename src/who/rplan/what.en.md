title: The R* Plan - What does it really mean?
----

The R\* Plan: a network of resistant communication
===========================================================================

What does it really mean for digital communication?
---------------------------------------------------

The R\* Plan\[[1](#1)\] consists in the distribution of several servers, and thus of services, among various
providers scattered around the world (we are always looking for providers or ISPs ready to host our computers).

Every server hosts all fundamental services offered by A/I and some particular services, as well a copy of the information needed to
re-create from scratch the whole structure (the most important part being the configurations and users database).

Public data (especially websites) are contained in all servers, so that it is very difficult to prevent users from accessing some particular
content.

Private data (especially mailboxes) are scattered among different servers, in order to limit any damages. We considered it more important to
grant privacy and the possibility of communicating, than to try and make endless copies of private mails placing them in all our computers.

If a server is compromised, all users will be moved to another server, without having to reconfigure anything: we will this try to grant
continuity in communication.

If all public servers are violated at the same time, this will mean that the situation is much more worrying. In that case, the
impossibility to send e-mails will only be a tiny inconvenient in a much more complex scenery. In that case, you can contact us at our Alpha
Centauri address, where we hope that by that time the much talked about better world will be ready.

Furthermore, all data (especially encryption keys) that can be used to decode messages will be always encrypted, and the computers will have
sensors to make sure that no violation has taken place. As usual, we will keep no logs (i.e. the data needed to identify connections from
and to our net) and no private data concerning our users.

The solutions we have sorted out aim at forcing, or at least at inviting, anybody wishing to access our servers to contact A/I, so that we
can know what they are planning to do, as well as how and for what reasons, and so we can grant the continuity of all communications.

Such things should be actually given for granted, but in our strange society, founded on fear towards our neighbours, they seem to become
something new, which must be invented and created from scratch.

Briefly, the R\* Plan aims at:

- Preventing any forceable communication blockage.
- Being able to decide about the modalities of any external intervention on our net (and therefore on the users' data).
- Offering our services to more and more people who are not willing to hand in their private communication to corporative sociologists (in
    the best case), by overcoming the physical limits of one only server.
- Doing anything possible, using imagination when needed, to protect our users' privacy.

The Turning Point
-----------------

We find it obvious that **this is not enough**. It undoubtedly creates better circumstances than those we have been enduring up to now, but
**the fight to oppose the equation "control+repression=solution to all problems" is much more complex than a simple technical solution**.

In these fearful years, the almost maniacal aggression to many people's fundamental liberties will maybe spread the defence of privacy and
freedom of speech. Or maybe not. In that case, all technical tricks we can make out will probably be useless.

Our hope is that more and more people do their part to learn and teach others about the use of digital and other instruments to stop the
threat to free communication and speech.

We have worked to improve our services, to make them more resistant, and we will keep doing so. But this can and must not be sufficient.
What we do not want is to create a false sense of safety, so that our users think they can totally rely on us for their privacy. The world
around us has already proved that any plan has a backdoor, and there are no unfailing tricks or plans that can free us from a steady
personal involvement in our private defence. If you want to learn more and find out what instruments you can use to protect your privacy,
read this [howto on e-mail privacy](/docs/mail/privacymail), which also contains some useful references.

- - -

**Note**

<a name="1"></a>\[1\] To develop our plan, we read through many history books, so as to remember the history of winners and find the best
strategy for a great leap forward. After reading the classics, from Adam Smith to Kipling, we bumped into an Italian text, a plan written as
late as 1982 by a former masonic Grand Master and aiming at subordinating Italy to corporations, and to turn citizens into numb disempowered
manpower That attempt was called R Plan, and since then everybody has been telling us it was thwarted. But all evidences around us show it
was not, and since it was so successful, even after 30 years, we have decided to get inspiration from it, but giving it a twist, calling it
R\* Plan.
