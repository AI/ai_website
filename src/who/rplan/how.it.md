title: Il Piano R* - Come funziona
----

Il Piano R\*: un network di comunicazione resistente
=============================================================================

Implementazione tecnica
-----------------------

**Pagina che necessita di aggiornamento**

Documentazione Tecnica: [Orange Book](http://www.autistici.org/orangebook/ "A/I Orange Book").

Questa sezione avrebbe la pretesa di spiegare a chi volesse conoscerlo il meccanismo con il quale stiamo realizzando la rete di server di
autistici.org/inventati.org (progetto meglio noto con il nome di "Piano R\*" :)

Il tentativo è di rendere fruibile questo passaggio a chi si diletta di tecnicaglie senza necessariamente essere un esperto.

**Layer zero : l'hardware**

Il piano R\* prevede la dislocazione di 'n' server in 'n' luoghi. L'assunto di base è che non esiste nessun modo
per impedire con certezza un accesso fisico non desiderato alla macchina, e che è quindi preferibile dotarsi di strumenti che possano
rilevare l'intrusione indesiderata e far scattare l'allarme per la compromissione di uno dei nodi della rete con l'idea di ridurre il danno
derivante da un'ipotetica manomissione fisica.

Resta inteso che questo non ci mette al riparo da eventuali intromissione remote, dalle quali evidentemente non si e' mai protetti
abbastanza.

**Layer uno : la rete**

Dovete immaginarvi la struttura reticolare tra i server come una rete ad anelli. A ciascun anello corrispondono regole di accesso
differenti, basate sulla tipologia dei servizi offerti. Gli anelli sono costruiti in base alla criticità del servizio, alla quantità di
connettività disponibile, alla collocazione fisica del server e alla tipolgia dell'hardware.

I server sono collegati tra loro attraverso una VPN realizzata mediante il software tinc. Tutte le comunicazioni tra i server, dalla
sincronizzazione all'indirizzamento della posta passano, crittate, attraverso la VPN.

**Layer due : la sincronizzazione dei servizi**

Uno degli obiettivi fondamentali del Piano R\* è quello di garantire che, nel caso in cui si venga costretti a
mettere off-line uno o più nodi (perché, per esempio, un nodo è risultato compromesso), i servizi offerti non vengano interrotti.

Per consentire questo è stato necessario strutturare un meccanismo di sincronizzazione dei materiali e di facile redirezione di tutte le
richieste fatte al server eventualmente compromesso verso un nuovo server.

Per sincronizzare le configurazioni dei vari servizi, il Piano R\* prevede l'uso di diversi meccanismi:

- [CFengine](http://www.cfengine.org/ "CFengine home page") è un software che consente di sincronizzare le configurazioni di diverse
    macchine, permettendo sia la sincronia di configurazioni valide per tutti i nodi di una rete, che configurazioni specifiche per ogni
    singolo nodo. Ogni singolo nodo conserva inoltre una copia di tutto il deposito di configurazioni, in modo che tutti i singoli server
    possano essere origine della sincronia, rendendo possibile l'installazione di un eventuale server di backup in un tempi
    relativamente veloci.
- Le informazioni relative a tutti gli utenti e ai loro servizi sono conservate nel database LDAP insieme ad alcuni dati (virtualhost
    per esempio) relativi alla configurazione di determinati servizi. Per comodità i file di configurazione (come quello di apache) vengono
    generati da script che recuperano le informazioni necessarie dal database LDAP. Gli script vengono aggiornati e sincronizzati
    tramite CFengine.
- Per gestire il database LDAP utilizziamo uno strumento autoprodotto che abbiamo chiamato Oliva.


**Layer tre : i contenuti degli utenti**

Per quanto riguarda i contenuti degli utenti e le porzioni più corpose dei contenuti dei servizi sul server, la sincronizzazione non era
realizzabile tramite CFengine, per via dell'elevato volume di dati che sarebbe stato necessario trasferire inutilmente.

I dati che è necessario sincronizzare su più nodi della rete (porzioni condivise di contenuti dei servizi, dati di alcuni utenti, chiavi e
certificati, ecc.) vengono trasferiti via rsync, come pagine html presenti in più copie, i servizi di backup e altre cosine.

Ogni casella di posta è fisicamente localizzata su un server, scelto in modo da bilanciare il carico della rete. È possibile, in qualsiasi
momento, spostare una determinata casella di posta da un server a un altro, modificando un parametro di LDAP. Questi spostamenti risultano
completamente trasparenti all'utente. Così come le caselle di posta, i siti web sono fisicamente disponibili su uno dei webserver del
network, con possiblità di essere spostati in breve tempo recuperando i dati dalle copie di backup, presenti su altre macchine della rete.

**Layer quattro : gli utenti**

Una delle novità più rilevanti del piano R\* è anche la localizzazione degli utenti. No,
non intendiamo dire che così gli utenti saranno facilmente identificabili, ma che con il piano R\* tutti gli utenti
saranno contenuti in un solo database LDAP (un database pensato per rendere al massimo in situazioni in cui è necessario leggere molte volte
dal database e scrivervi raramente).

Nel database LDAP sono contenute tutte le informazioni degli utenti, nonché le informazioni relative ai vari servizi collegate a ogni
utente (dove risiede la sua casella di posta o il suo sito, la sua password, ecc. ecc.).

**Layer cinque : i servizi**

Per comprendere la parte tecnica di realizzazione del piano R\* vi manca solo un'idea di come i vari servizi siano
organizzati tra i vari nodi.

In genere ogni servizio è pensato per essere distribuito (normalmente utilizzando round robin per smistare le richieste) su tutti i nodi, e
allo stesso tempo per non dipendere in particolare da nessun nodo specifico. Salvo l'impossibilità di realizzare, per alcuni servizi, questo
schema.

Vediamo i principali servizi:

- I database mysql sono replicabili su tutte le macchine. Non tutti i database sono replicati, ma solo quelli che devono essere presenti
    su tutti i nodi per un qualsiasi motivo. I database dei singoli utenti sono di solito ospitati solo sul server dove è ospitato anche il
    loro sito web.
- Il server web (e il server ftp di conseguenza) è configurato per rispondere su ogni nodo. I siti degli utenti sono divisi sui vari nodi
    e la redirezione viene effettuata automaticamente dal server una volta che si cerca di raggiungere il sito. Ovvero: il dominio
    www.autistici.org risolve in round robin su tutti i nodi della rete, e ognuno dei nodi redirige la richiesta sul nodo che effettivamente
    ospita il sito.
- Il server di posta in uscita (smtp) è anch'esso configurato su ogni nodo indipendentemente. Ogni nodo può consegnare la posta e di fatto
    riceve una percentuale identica di messaggi (il campo MX del dominio autistici.org è configurato per distribuire i messaggi su tutti
    i nodi). Ogni nodo poi consegna il messaggio alla macchina che effettivamente ospita la casella di posta dell'utente.
- Le caselle di posta degli utenti sono distribuite sui vari nodi. Il server di ricezione della posta (POP/IMAP) è configurato per
    rispondere su ogni nodo e per girare la richiesta alla macchina corretta grazie all'uso di un proxy (Perdition).
- Le mailing list invece sono centralizzate su una sola macchina (anche se le loro configurazioni vengono copiate per sicurezza su tutti i
    nodi della rete, in modo da poter ripristinare il servizio di mailing list in maniera estremamente rapida): questa macchina ospita gli
    archivi e tutto il sistema di consegna di posta relativo alle mailing list.

 In sostanza, ogni singolo nodo della rete serve parte dei siti e parte delle mail. In un qualsiasi momento se uno dei nodi viene manomesso,
tutte le sue configurazioni e la parte di siti e caselle che gestiva vengono trasferite a un altro nodo della rete, impedendo un breakdown
della comunicazione.
