title: The R* Plan
----

The R\* Plan: a network of resistant communication
===========================================================================

![](/static/img/pianor_rete.png)

**R\* Plan**: a network of resistant communication

The [A/I collective](/about) is glad to introduce you the **R\* Plan**.

The **R\* Plan** is a tool to develop a network of resistant communication.

Resistant because it has been conceived to prevent as much as possible (but without feeling almighty) breakdowns in the digital
communication offered by our facilities.

And resistant also because it is related to the dream of a real political and social conflict that lives on and needs communication tool to
spread and flourish.

We are opening this new site because:

- we would like to tell you how the R\* Plan was conceived and developed,
- we would like to explain what led us to develop the Plan as it is now and what events convinced us that it was urgent,
- it is fundamental to make the transition to the new structure as easy as possible, and therefore we will try to give you simple
    instructions to face the change as easily as possible, and to enjoy the novelties :)

Everyone can contribute to the project: if you wish to suggest us any improvements or considerations, write to <info@autistici.org> (using
the gpg key you'll find [here](/get_help#gpgkey "A/I GPG key") would be ideal). But a greater contribution to
everybody's privacy consists in never relying on others for your security: we can offer the safest and most private connections possible,
but the greatest security is the one you ensure on your own. That is why we'll never stop repeating that the safest way to protect your
communication privacy is encryption.

If you want to know more about this topic, you can start reading this [howto](https://help.riseup.net/en/security/message-security): by
spreading secure communication habits, you will most considerably help safeguarding everyone's privacy, which is a fundamental requirement
for **opposing total control**, and you will also turn our communication networks into an instrument of **fight and dissent**.

Let's start the tour:

- [Introduction to the R\* Plan](/who/rplan/intro "introduction to the R* Plan")
- [How does R\* Plan work](/who/rplan/how "R* Plan mechanics")
- [What does R\* Plan mean](/who/rplan/what "R* Plan reasons")
- [Slides summarizing the R\* Plan](http://www.autistici.org/orangebook/slides/orangebook.en.html "R* Plan slides")

The A/I collective

October 2005
