title: A short tale about why we are who we are and why we do what we do
----

A short tale about why we are who we are and why we do what we do
=================================================================

When we wrote [our first manifesto](/who/manifesto), the world was a fairly
complicated place to live our days off. After two World Wars, two A-bombs,
thirty year of cold war and a state of more or less permanent war, we carried on
dipped in a man-eating system based on money. Our perspectives were limited and
our future set into the shape of a snake eating its own tail. In those dark
times A/I was a place for us to resist and to survive. We wanted to contribute
the things we were able to do to the small, headstrong and determined community
of those who could not accept the games of power and money.
We called ourselves autistici, or paranoid, because we were well aware of how
our attitude towards technology, and our innate distrust of the liberal
rhetorics of representative democracy and its repressive groundlessness, were
radical for those years -- times of optimism and compromise with the abuses of
global capitalism.

We got it back then, almost 20 years ago. Today we are among the few providers
of services that respect their users' privacy and equate it to a political
claim, and if we do what we do it is because the system that systematically uses
technology to surveil and control our lives (when it isn't exploiting our
private communications and our sensitive data on behalf of multinational
companies) has ruinously lost its mask. Since Snowdens leaks started to become
public, our community has faced a flood of scared individuals who were embittered
and suddenly (or finally, perhaps) disillusioned. But at the same time we see
too often a need to find a simple and quick solution, a safe harbour that is
once again the outcome of delegation, which is an utilitarian and irresponsible
process.

But just as when we started, our approach is first and foremost political,
radical and stubbornly anti-authoritarian. From start, we have been forced
to define ourselves by negative statements, because the things happening around us
were so disgusting it was necessary to keep them at a distance without any
uncertainty: therefore **antifascists, antiracists, antisexists, antimilitarists**.
In our harbour, protection is offered only to those who dare opposing those
hating winds, which unfortunately are blowing all over Europe.

And of course it was necessary to state very clearly our
nature was (and is) **non-commercial**, since we wanted to make clear that A/I
represented time stolen from our daily toil, from the precarious and wearying
torments of a job, that it stood for the joy, the sweat, the hardwork and tears
of the struggles we participated into against a world shaped into a coin.

In these flexibly painful times mankind is replaced by human capital (where, to
be sure, capital is the noun and human just and adjective). Being forced to
take a side on such a stage, our primary need is to unite and contribute
with our skills and ideas to the survival of critical thinking and of our very
lives.

Technique comes from the ability of man to use its opposable thumb but
in these times it has completed its transformation into religious belief, becoming a
strategic sector for an endless process of control and growth that is both deceptive and
suicidal.

The cycle of life: consuming resources to produce objects which will
soon become rubbish. Data are gathered to produce desiders, profit and power,
and to create fear, self-censorship, resignation. Quite an experience to live
in fear, isn't it? That's what it is to be a slave.

Maybe what we wanted to tell to the people was just: don't be
afraid, stand up; dignity, determination, bravery, self-organization. And these may be the only words in
our manifesto that really matter.
