title: Don't panic!
----

Don't panic!
============

Autistici/Inventati help page
-----------------------------

In this page we try to tell you what you can do when you think you need our help. We've set up some instruments to try to reduce support
email traffic. So, before writing us, **you're strongly invited to try and find the solution to your problem here**.

- [Cavallette](https://cavallette.noblogs.org/) - News?
- [FAQ](/docs/faq/) - a series of frequently asked questions (and their answers of course! :)
- [Manuals](/docs/) - a collection of manuals on how to configure various clients to use our services
- [Helpdesk](https://helpdesk.autistici.org/) - here you can leave a specific communication concerning your problem and wait for an answer that will come as soon as we have time (of course you will have to leave an email contact to receive the answer)!

It is also generally possible to find somebody of us lurking in the **\#ai** channel of the [autistici.org IRC server](/docs/irc/).
People there might be willing to help with small technical issues and other quick things.

<a name="gpgkey"></a>

If you are really desperate -- but before resorting to this you should be on the verge of a horrible death -- you can
[write us](mailto:info@autistici.org), possibly using our [GPG key](gpg_key) (available on key servers worldwide)

