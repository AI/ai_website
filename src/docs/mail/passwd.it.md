title: Come cambiare o recuperare la password della propria mailbox
----

Come cambiare o recuperare la password della propria mailbox
=====================

A/I ha preparato un servizio per i più smemorati, che permette di recuperare la propria password rispondendo ad una semplice domanda...

Dato che la domanda di default riguarda gli animali domestici il servizio si chiama GATTI.

Ovviamente è <u>importantissimo</u> impostare la propria risposta prima di perdere la password!

Si imposta attraverso il [pannello utente](/pannello/)

- - -

Come recuperare la Password
---------------------------

### Step 1 - Arriva il gatto

Il gatto viene in tuo aiuto automaticamente quando sbagli la password. Vai sulla pagina di [login](https://accounts.autistici.org), prova a inserire il
tuo nome utente e la password (che sarà sbagliata visto che non te la ricordi) e a entrare. A questo punto ti compare un nuovo link "dimenticato la password ?", 
cliccalo per andare alla pagina del gatto.

### Step 2 - Rispondi alla domanda del gatto

Adesso appare la domanda che avevi impostato in precedenza. Una volta inserita la risposta corretta, compare il messaggio:

*Ok. Your new password is RtYcYEFnVMvu However, you will need to change it before you can use it again...*

che significa: la nuova password è *RtYcYEFnVMvu* ma DEVI cambiarla per accedere alla casella di posta.

### Step 3 - Cambia la password!

Per cambiare la password è necessario entrare nel pannello utente e seguire [l'apposito link](/docs/userpanel#passwordchange).
