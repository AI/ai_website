title: Notas técnicas sobre el servicio de correo 
----

Notas técnicas sobre el servicio de correo.
==================================================

En la parte del usuario, el sistema de correo de A/I está totalmente descentralizado: si quieres leer y enviar tus e-mails, puedes
conectarte a cualquier servidor de la red. Esa es la función de nuestras direcciones especiales `mail.autistici.org` y `smtp.autistici.org`,
que "clasifican" las conexiones entre los servidores de una manera más o menos aleatoria.

De acuerdo al [Plan R\*](/who/rplan/), en cualquier momento tu casilla de correo está materialmente ubicada es un servidor de
la red de A/I.

Por esta razón, tu casilla puede resultar inaccesible debido a un problema temporal de conexión; cuando esto sucede, tu correo entrante
**NO** se perderá, pero quedará en espera en otros servidores de red. Si el problema no es transitorio, tu cuenta de correo será movida a un
servidor diferente y pronto podrás leer tu correo nuevamente (para acceder a tus archivos deberás esperar a que el servidor afectado sea
restablecido).

Parámetros de conexión.
-----------------------

Deberías haber recibido los parámetros necesarios para conectarte a tu servidor de correo a través del e-mail que recibiste cuando tu cuenta
fue activada. Sin embargo, puedes encontrarlos [aquí](/docs/mail/connectionparms).

Enviando correos.
-----------------

Últimamente se nos ha informado sobre algunos problemas con mensajes enviados a través de varios ISPs (Telecom Italia, por ejemplo). Esto se
debe principalmente a que cada vez más proveedores eligen bloquear conexiones privadas salientes que pasen por el puerto 25, en un absurdo
intento de reducir el envío de spam.

Nos gustaría recordarte que, cuando sea posible, los puertos recomendados para el envío de correos son:

- **465**, con SSL
- **587**, con TLS

Filtros anti-spam.
------------------

A/I implementa en sus servidores algunos mecanismos resueltos a reducir el spam entrante. Ya que no podemos configurar estos filtros para
cada usuario, por lo general son muy restrictivos (esto quiere decir que permiten el paso de tantos correos como sea posible). Los mensajes
que consideramos como spam son puestos en la carpeta "Basura" que está en la casilla de todos los usuarios. Esta carpeta se vaciará cada 30
días.

Si verificas tu casilla a través del **webmail** o a través de un cliente de correo **IMAP** podrás verificar que los mensajes en la carpeta
"Basura" son realmente lo que crees que son. Y si nuestro software estuviera equivocado podrás poner el mensaje nuevamente en tu Bandeja de
entrada.

Pero si sólo chequéas tu casilla vía un cliente de correo **POP** no podrás ver el contenido de la carpeta "Basura". Esto quiere decir que
podrías perder mensajes que no son realmente Spam pero que parecían serlo.

Para evitar este malentendido debes elegir una de las siguientes soluciones: o bien deberás verificar rutinariamente tu casilla a través
del **webmail** dejando la carpeta "Basura" fuera, o bien tendrás que [configurar un filtro](/docs/mail/sieve) en tu **webmail** que
enviará todos tus supuestos mensajes de spam nuevamente a tu Bandeja de entrada, donde podrás decidir qué hacer con ellos.

Debido a algunos de nuestros sistemas anti-spam, algunos mensajes pueden ser entregados con algunos minutos de retraso (pero recuerda que
con los e-mails, nunca hay que dar por sentado la entrega inmediata: mejor piensa en esto antes de preocuparte por un posible
malfuncionamiento de los servicios de correo de A/I)...
