title: Come configurare Thunderbird/Icedove
----

Come configurare Thunderbird/Icedove
========================================

Queste istruzioni si riferiscono a Thunderbird 9, con altre versioni potrebbe cambiare qualche particolare nelle opzioni.

Scaricati Mozilla Thunderbird da [qui](https://www.mozilla.org/it/thunderbird)

Apri Thunderbird e clicca su "Crea Nuovo Account"

![](/static/img/man_mail/it/thunderbird-it-01.png)

Accanto a "Nome" scrivi il nome che vuoi far comparire sulle mail che scrivi, poi inserisci il tuo indirizzo e-mail, la password (e lascia
il segno di spunta su "ricorda password" se vuoi che la tua password sia salvata sul disco per non digitarla ogni volta) e clicca su
"Continua".

![](/static/img/man_mail/it/thunderbird-it-02.png)

Puoi scegliere tra IMAP e POP3, noi consigliamo POP3 in modo da scaricare la posta sul proprio computer, ad ogni modo puoi scegliere anche
IMAP se preferisci questa modalita', dopodiche clicca su Configurazione Manuale .

![](/static/img/man_mail/it/thunderbird-it-03.png)

Nel campo "Nome utente" scrivi il tuo indirizzo di posta completo, poi clicca su "Crea un account".

![](/static/img/man_mail/it/thunderbird-it-04.png)

A questo punto l'account e' configurato, per rivedere le impostazioni puoi cliccare su "Modifica" e poi "Impostazioni Account", nella
colonna di sinistra clicca su "Impostazione Server" e dovrebbe comparire una finestra come questa:

![](/static/img/man_mail/it/thunderbird-it-05.png)

Controlla le informazioni riportate nella finestra. Se ci sono errori, modifica quel che c'e' da cambiare. Quando hai finito, clicca su
"OK".

Poi, sempre nella colonna di sinistra, clicca su "Server in Uscita (SMTP), comparira' una finestra come questa:

![](/static/img/man_mail/it/thunderbird-it-06.png)

In alto a destra puoi cliccare su Modifica e cambiare i parametri nella finestra, e' importante che il nome utente sia l'indirizzo email
completo:

![](/static/img/man_mail/it/thunderbird-it-07.png)
