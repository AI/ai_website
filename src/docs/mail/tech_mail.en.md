title: Technical note on e-mail service
----

Technical notes about e-mail service
========================================

On the user's side, A/I's mail system is totally decentralised: if you want to read and send your e-mail, you can connect to any server of
the network. That's the function of our special addresses `mail.autistici.org` and `smtp.autistici.org`, which "sort" connections among the
servers more or less randomly

According to the [R\* Plan](/who/rplan/), at any given moment your mailbox is materially located on one server of
the A/I network.

That's why your mailbox can happen to be unaccessible due to a temporary connection problem; when this happens, your incoming mail
**won't** get lost, but will stay queued in the other network servers. If the problem is not transitory, your mail account will be moved to
a different server and you will be able to read your new mail very soon (to access your archives you'll have to wait for the affected server
to be restored).

Connection parametres
---------------------

You should have received the necessary parametres to connect to your mail server through the mail you received when your account was
activated. However, you can find them [here](/docs/mail/connectionparms).

Sending mail
------------

We have lately been informed about some problems with messages sent through several ISPs (Telecom Italia, for instance). This is mostly
because more and more providers are choosing to block private outgoing connections passing through port 25, in an awkward attempt to reduce
spam sending.

We would like to remind you that, whenever possible, the recommended ports for mail sending are:

- **465**, with SSL
- **587**, with TLS

We would like to remind you as well that sending serveral mail to large number of recipients is not possible from single user mail accounts.
If you need to do such a thing, please ask us a newsletter!

Anti-spam filters
-----------------

A/I implements on its servers some mechanisms sorted out in order to reduce incoming spam. Since we cannot possibly configure these filters
for each single user, they are generally very conservative (i.e. they allow the passage of as many messages as possible). Messages we
consider spam are dropped into a "Spam" folder in every user's mailbox. This folder will be cleared every 30 days.

If you check your mailbox via **webmail** or via **IMAP** mail client you will be able to verify that messages in the "Spam" folder are
really what we think they are. And if our software was mistaken you will be able to put the message back into your INBOX.

But if you only check your mailbox via a **POP** mail client you won't be able to see the content of the "Spam" folder. This means you
could lose messages that were not actually spam but only seemed to be ones.

To avoid this misunderstanding you have to choose one of the following solutions: either you routinely check your mailbox via **webmail**
sorting the "Spam" folder out, or you'll have to [configure a filter](/docs/mail/sieve) on your **webmail** that will
sort all your supposedly spam messages back into your INBOX, where you will be able to decide what to do with them.

Due to some of our anti-spam systems, some messages may be delivered with a minutes delay (but remember that with e-mail instant delivering
is never granted: you'd better think about this before worrying about a possible disfunction in A/I's mail services)...
