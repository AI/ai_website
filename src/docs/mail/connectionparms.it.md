title: Parametri per la connessione ai server di posta
----

Parametri per la connessione ai server di posta
===============================================

Riepilogo dei parametri necessari a configurare un client di posta sul proprio PC per leggere e inviare la posta utilizzando un account su
Autistici/Inventati (supponendo che tale mailbox sia *utente@inventati.org*):

<table>
<tbody>

<tr>
<th>
Server
</th>
<td>
mail.autistici.org
</td>
</tr>

<tr>
<th>
Protocollo
</th>
<td>
POP3 or IMAP4
</td>
</tr>

<tr>
<th>
Porta
</th>
<td>
per POP3: 110 (STARTTLS) o 995 (SSL)<br>
per IMAP4: 143 (STARTTLS) o 993 (SSL)
</td>
</tr>

<tr>
<th>
Username
</th>
<td>
<i>utente@inventati.org</i>
</td>
</tr>

</tbody>
</table>


## Outgoing mail

<table>
<tbody>

<tr>
<th>
Server
</th>
<td>
smtp.autistici.org
</td>
</tr>

<tr>
<th>
Porta
</th>
<td>
587 (STARTTLS) o 465 (SSL)
</td>
</tr>

<tr>
<th>
Username
</th>
<td>
<i>utente@inventati.org</i>
</td>
</tr>

</tbody>
</table>

<a name="POP/IMAP"></a>

# POP oppure IMAP?

POP3 e IMAP4 sono i due protocolli standard per la lettura della posta su Internet: ambedue possono essere usati per leggere la vostra
casella di posta su A/I, sceglierne uno corrisponde a scegliere una modalità di utilizzo della vostra mailbox:

Il protocollo POP è stato pensato per fare una cosa sola: scaricare i nuovi messaggi da un account remoto. Usando POP si può utilizzare
la propria mailbox su Autistici alla stregua della cassetta di posta condominiale: quando arriva della posta nuova, la prendete e la
portate in casa. Questa è la modalità di utilizzo che preferiamo, perché mantiene basso l'utilizzo di risorse sui server e consente a
ciascuno di decidere della riservatezza della propria corrispondenza autonomamente.

Il protocollo IMAP invece è studiato per consentire la gestione di un archivio di posta remoto, ad esempio consentendo la creazione di
cartelle dove archiviare i propri messaggi. In questo caso tutta la posta rimane sul server, il che pone dei limiti allo spazio totale
utilizzabile, pero' puo' essere l'unica soluzione nel caso non si disponga, ad esempio, di un proprio computer, oppure si desideri avere
sempre la propria posta a disposizione tramite la webmail.

Questi, a grandi linee, sono i due possibili modi di usare la propria mailbox su A/I. E' anche vero che non è strettamente una questione di
protocollo, dato che è possibile mantenere i propri messaggi sul server anche con POP, e scaricarli sul proprio computer con IMAP....


## Il mio client di posta non supporta SSL

I client di posta meno moderni potrebbero non supportare l'ultima versione di
SSL, e in questi casi vi raccomandiamo di passare a un client più recente. Come
vedrete di seguito, la procedura per risolvere il problema, infatti, è convoluta
e parecchio scomoda. Ma se non potete cambiare il vostro client di posta, è
comunque possibile usarlo con una connessione sicura. Quel che dovrete fare è
*incapsulare* la connessione email non crittata in una connessione crittata. I
programmi che si possono usare per farlo sono tanti, e tra questi c'è *stunnel*.

Stunnel è un software libero liberamente scaricabile, e per usarlo 
[scaricate la configurazione di stunnel](/static/data/ai-stunnel.cfg) 
e lanciate stunnel con il comando:

    stunnel ai-stunnel.cfg

A questo punto, le porte disponibili sul vostro computer saranno diverse, e
tutto quel che dovete fare è modificare la configurazione del vostro client per
usare una di quelle porte invece di quelle standard. In particolare:

* POP: 19110
* IMAP: 19143
* SMTP: 19025

Se volete chiudere stunnel quando non vi serve, usate questo comando:

         kill $(cat /var/run/ai-stunnel.pid)

E naturalmente ricordate che d'ora in poi quando dovete spedire e ricevere la
vostra posta elettronica avrete bisogno di lanciare stunnel!
