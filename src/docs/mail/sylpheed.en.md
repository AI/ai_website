title: Sylpheed Configuration Howto
----

Sylpheed Configuration Howto
================================

    Version: 0.1.2
    Updated: 15-09-2005
    Keywords: Slackware 10.1,sylpheed 1.04
    Description: Use Sylpheed to send and download mail from A/I via SSL
    Language: en (English)

This mini Howto will teach you how to configure sylpheed mail client to download A/I mail from our servers using SSL.

From the configuration panel, choose *Create New Account*

Then fill in your [connection parameters](/docs/mail/connectionparms) as in the following pictures. Unfortunately
pictures are in Italian but we guess you can find your way around. Change *tuonome* with your full email address.

![](/static/img/man_mail/it/posta_sylpheed1.jpg)

![](/static/img/man_mail/it/posta_sylpheed2.jpg)

![](/static/img/man_mail/it/posta_sylpheed3.jpg)

![](/static/img/man_mail/it/posta_sylpheed4.jpg)

