title: How to change or recover your mail password
----

How to change or recover your mail password
===================

A/I offers a service for those of you with very bad memory, that allows you to recover your password by answering a simple question...

It is **very important** you set your answer before losing the password! (we suggest doing it at soon as your account is activated).

How to recover your password
----------------------------

### Step 1 - Click on "Forgot your password?" in the login form

Go to the [login](https://accounts.autistici.org) page and click on
"Forgot your password?" under the login form.

### Step 2 - Answer your recover question

Now you see the question you set when you first got your password. Once you enter the correct answer, you will see a big title saying:

*Ok. You new password is RtYcYEFnVMvu However, you will need to change it before you can use it again...*

This means you have a new password, that is: *RtYcYEFnVMvu* and that you MUST change it to use the mailbox.

### Step 3 - Change your password!

To change your password, just log into your user panel and [follow the right link](/docs/userpanel#passwordchange)!
