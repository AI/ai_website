title: Come creare una password sicura
----

Come creare una password sicura
===============================

Le password sono spesso l'anello debole in ogni sistema di sicurezza in cui vengono utilizzate. Per questo motivo, il primo passo per aumentare la propria sicurezza e' quello di apprendere alcune tecniche basilari per creare una buona password.

Cose da evitare:

*	**Non utilizzate la vostra data di nascita, ne' una parola di uso comune, ne' un nome proprio**. Spesso una password viene violata con facilita' proprio perche' la maggior parte delle persone tende a sceglierne una che e' la variante di un termine presente nel dizionario. Il linguaggio umano e' composto da un numero molto ridotto di vocaboli. Usare un computer per provare in serie ogni combinazione possibile di caratteri, simboli e lettere fino a trovare quella effettivamente corrispondente alla nostra password e' un'operazione banale da compiere per un computer. Questo e' vero anche per quelle parole in cui le lettere sono state sostituite con dei numeri. Per esempio, la password "L0V3" e' facile da violare tanto quanto "LOVE"

*	**Non usate la stessa password per tutti i vostri account**. Potrebbe essere meglio scriversi le proprie password e conservarle in luogo sicuro, piuttosto che usare sempre la stessa.

*	**Non dimenticate di cambiare la vostra password con regolarita'**. Dovreste farlo almeno una volta all'anno.

*	**Non dite mai a nessuno la vostra password**, sopratutto se ve la chiedono.


In che modo allora creare una password che sia robusta e allo stesso tempo semplice da ricordare? Non e' un problema semplice da risolvere. I metodi generalmente consigliati sono tre:

**1. Usate un password manager**

Non provate neppure a ricordare la vostra password. Invece generatene di casuali per ognuno dei siti e dei servizi che utilizzate e salvatele in un password manager sicuro. In rete potete trovarne diversi. Vi consigliamo [KeepassX](https://www.keepassx.org/): e' open source e multipiattaforma (disponibile per Linux, Mac e Windows).


**2. Create parole chiave complesse**

a. Pensate a **piu' parole** che potete ricordare facilmente.

b. Prendete le lettere iniziali (o quelle finali) di ognuna di queste parole e create una **parola priva di senso**.

c. Aggiungete in maniera casuale delle **lettere maisucole**, dei **simboli** o dei **numeri**.

Ad esempio:

_"Creare Saperi Senza Fondare Poteri"_ diventa _"cssfp"_. Fatto questo aggiungete alcune lettere maiuscole (_"cSsfP"_) e poi altre lettere e numeri all'inizio ed alla fine della parola. Il risultato finale potrebbe essere _"84cSsfP#!"_


**3. Create frasi chiave**

a. Prendete alcune parole casuali che riuscite facilmente a ricordare, per esempio _autistici_ + _crypto_ + _resist_ + _paranoia_. Per aumentare la complessita' della password utilizzate parole in lingue differenti.

b. Unite tutte queste parole in un unica frase: _"autisticicryptoresistparanoia"_. Si tratta di una password lunga ma facile da ricordare.

c. Se volete ulteriormente aumentare la complessita' della password inserite anche dei termini non presenti in alcun dizionario (tipo _"autisticicryptoresistparanoia7grln"_)

d. Non create password piu' lunghe di 60 caratteri perche' questo potrebbe creare dei problemi per leggere la posta.

Ecco un altro esempio: 

![](/static/img/man_mail/it/passwd_safe.png)
