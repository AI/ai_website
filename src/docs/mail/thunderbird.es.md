title: Como Configurar Thunderbird
----

Como Configurar Thunderbird
=================================

En primer lugar usted necesita descargar e instalar Mozilla Thunderbird [aquí](http://www.mozilla.org/products/thunderbird/all.html).

1) A continuación, siga las siguientes instrucciones

2) Abre Thunderbird y haga clic en Editar en la barra de menú, a continuación, seleccione Configuración de la cuenta.

![](/static/img/man_mail/en/tbA.jpg)

3) En la ventana *Configuración de la cuenta*, haga clic en *Agregar cuenta*

![](/static/img/man_mail/en/tbB.jpg)

4) Cuando aparezca la ventana del *Asistente de cuenta*, haga clic en el círculo junto a la cuenta de correo electrónico y, a continuación,
haga clic en *Siguiente*.

![](/static/img/man_mail/en/tbC.jpg)

5) Introduzca los valores deseados para su *nombre* (el nombre que desea que aparezca en todos los correos electrónicos que envíe) y la
*dirección de correo electrónico* y, a continuación, haga clic en *Aceptar*.

![](/static/img/man_mail/en/tbD.jpg)

6) En la página *Información del servidor*, haga clic en el círculo al lado de *POP*. En el campo situado junto a *Servidor entrante*
escriba "mail.autistici.org". A continuación, haga clic en *Siguiente*.

7) En el campo *Nombre de usuario entrante* debe tener su dirección de correo electrónico ingresada. O ingrésela en el cuadro y luego haga
clic en *Siguiente*.

![](/static/img/man_mail/en/tbE.jpg)

8) En el cuadro *Nombre de la cuenta* debe ser llenado con su dirección de correo electrónico. Este valor se utiliza sólo para identificar
la cuenta, puede dejarlo como predeterminado o cambiarlo si lo desea. Cuando la configuración esté correcta , haga clic en *Siguiente* para
continuar.

![](/static/img/man_mail/en/tbF.jpg)

9) Revise la información mostrada. Si aparecen errores, haga clic en *Atrás* para corregirlos, como se muestra en los pasos anteriores.
Cuando termine haga clic en *Finalizar*.

![](/static/img/man_mail/en/tbG.jpg)

10) En la pantalla *Configuración de la cuenta*, en la ventana izquierda, seleccione la cuenta que acaba de crear y haga clic en el signo
más para expandir el menú. Cuando el menú está desplegado, haga clic en *Configuración del servidor*, seleccione la casilla de *Usar
conexión segura (SSL)* y verifique que el *puerto* está en la lista de "995".

![](/static/img/man_mail/en/tbH.jpg)

11) La misma operación debe repetirse para el SMTP, el servidor de gestionar el envío de correos electrónicos. Haga clic en *Servidor de
salida* en la ventana izquierda y escribir "smtp.autistici.org" en la ventana *Nombre del servidor* y su dirección de correo electrónico en
la ventana de *nombre de usuario*. A continuación, seleccione la opción SSL y verifique que el *puerto* está en la lista como "465". Si ha
seleccionado la opción TLS, la opción de *puerto* puede ser establecido como "25".

![](/static/img/man_mail/en/tbI.jpg)
