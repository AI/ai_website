title: Noblogs Blogging Howto
----

Noblogs Blogging Howto
======================

This section of A/I Howtos is dedicated to bloggers and wannabe web editors: docs hosted here are meant to be an introduction to using blogs
and to privacy issues connected to blog publishing and to web surfing generally speaking.

If you are looking for a manual on how to use Wordpress (the software on which [Noblogs.org](http://noblogs.org) runs), please refer to
**[the official WP manual](http://codex.wordpress.org/)**.
