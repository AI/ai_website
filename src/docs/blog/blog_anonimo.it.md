title: Blog e anonimato
----

Blog e anonimato
================

Questa è una piccola guida tecnica per coloro che vogliono mantenere l'anonimato quando gestiscono o usano un blog, e più che agli esperti è
destinata a chi vive in situazioni dove è pericoloso criticare le autorità o a chi non vuole rischiare il posto di lavoro criticando il
padrone.

Per mantenere l'anonimato quando si usa un blog è necessario, per prima cosa, non pubblicare cose che possano far risalire all'autore: anche
usando il sistema più sicuro, se pubblico qualcosa che posso sapere solo io, sarà facile risalire a me.

Ma è possibile individuare la vera identità di un blogger anche attraverso le "tracce" che lascia quando usa Internet: qualsiasi computer
collegato alla Rete ha un numero che lo identifica (indirizzo IP) ed è facile risalire da questo numero alla persona che ha scritto un
commento o pubblicato un post.

Non è sempre semplice, ma qualcosa si può fare per garantirsi una maggiore riservatezza; le soluzioni che seguono vanno, dal punto di vista
tecnico, dalla più facile alla più difficile e dalla meno sicura alla più sicura.

### 1. registrarsi a un servizio di webmail con uno pseudonimo o usare un weblog pubblico

Aprire una casella di posta elettronica o un weblog usando uno pseudonimo è semplice, ma è anche facile risalire al suo reale utilizzatore,
proprio grazie all'indirizzo IP di cui abbiamo parlato sopra, specialmente se ci si collega a Internet dal computer di casa o di lavoro.

### 2. usare i computer pubblici

Usare i computer di un Internet point (un Internet Café) significa usare Internet attraverso un IP che è quello del computer pubblico e non
il nostro. Questo potrebbe aiutarci a manterere l'anonimato, ma se per usare uno di questi computer ci si deve registrare con un documento
di identità, cosa che in Italia è imposta dalla legge e che si può evitare solo in pochi posti, si torna al punto di prima. Questa
registrazione è obbligatoria in alcuni paesi dittatoriali del Terzo Mondo e in Italia.

### 3. usare un proxy server pubblico

Visto che il sistema per mantenere l'anonimato è quello di usare Internet con un IP che sia diverso da quello del nostro computer, è
possibile sfruttare, anche da casa o dal lavoro, i proxy server pubblici. In questo modo l'indirizzo IP che usiamo sarà quello del proxy
server e non il nostro.

Ecco una lista di alcuni dei siti che contengono elenchi di proxy server che si possono usare:

- [http://publicproxyservers.com (anonimi e non anonimi)](http://publicproxyservers.com)
- [http://www.samair.ru/proxy/ (solo anonimi)](http://www.samair.ru/proxy/)
- [http://tools.rosinstrument.com/proxy/ (motore di ricerca per proxy)](http://tools.rosinstrument.com/proxy/)

Una volta trovato il proxy server che fa al caso nostro, occorre impostare le preferenze del nostro browser, inserendo l'indirizzo IP del
proxy (un numero del tipo 125.125.125.125) la porta (un numero del tipo 8080) e salvarle. A questo punto quando useremo il browser l'IP
usato sarà quello del proxy e non il nostro. Questo però renderà l'uso del browser più lento, in quanto la richiesta di una pagina deve
passare per il proxy server prima di arrivare a destinazione invece che andarci direttamente.

Chi usa Firefox (cosa che consigliamo) deve inserire i dati del proxy andando nel menu "Preferenze" e scegliendo il tab "Generale" e quindi
premendo il pulsante "Connessione" (in basso a destra) a questo punto compare una finestra dove inserire i dati del proxy server.

Ma anche questo sistema potrebbe essere bloccato da un governo particolarmente irrispettoso della riservatezza, e in questo caso sarebbe
impossibile per noi collegarci a un proxy pubblico.

### 4. usare un proxy server personale

Nel caso sia impossibile usare un proxy server pubblico, è sempre possibile usarne uno "privato". In poche parole basta avere un amico che
ha un computer sul quale gira un proxy e collegarsi a Internet tramite questo. Naturalmente il computer dell'amico dovrebbe essere al riparo
dalle ritorsioni in quanto collocato all'estero e/o in un paese dove non è possibile che arrivi la lunga mano del nostro governo.

Uno dei programmi che possono servire a questo scopo è "circumventor", un'applicazione che (purtroppo) funziona solo con Windows e che non è
semplicissimo da installare, le istruzioni (in inglese) si trovano
[qui](http://www.peacefire.org/circumventor/simple-circumventor-instructions.html)

Anche in questo caso, come nel precedente, l'uso di Internet diventerà un po' più lento: questo dipende dalla velocità di connessione, in
quanto la richiesta di una pagina deve passare per computer dove è installato "circumventor" prima di arrivare a destinazione.

Ma, in questo caso, a meno che il nostro amico non ci tiri qualche brutto scherzo, sarà più difficile che il nostro anonimato venga violato.

### 5. usare Tor

["Tor"](/docs/anon/tor) è un sistema che permette un alto grado di riservatezza, in quanto il nostro collegamento a Internet
passa attraverso diversi computer, il che rende molto più difficile risalire a noi. In più tutti questi passaggi sono crittati, vale a dire
che è anche difficile capire cosa sta transitando sulla Rete per qualcuno che provi a "spiare".

Le istruzioni per installare "Tor" si trovano [qui](https://tor.plentyfact.net/index.html.it)

"Tor" funziona su tutti i principali sistemi operativi ed è abbastanza semplice da installare.

I problemi per chi usa "Tor" sono gli stessi di chi usa un proxy server, vale a dire che l'uso di Internet è più lento e che inoltre alcuni
siti web potrebbero impedire l'uso di alcune funzioni (per esempio la Wikipedia blocca i tentativi di editare gli articoli a chi usa "Tor").

Anche "Tor", che è un particolare tipo di proxy, può essere bloccato da chi gestisce la Rete.

### Per finire

Questa breve guida non voleva essere esauriente ma solo dare un'idea di quali strumenti abbiamo a disposizione per mantenere la
riservatezza. Come abbiamo già ricordato il grado di riservatezza sarà proporzionale al tipo di strumento che scegliamo di adoperare.
