title: Xabber Jabber client configuration Howto
----

Xabber Jabber client configuration Howto
===========================

    Considering the e-mail address user@domain.org
    User: user
    Server: domain.org
    Check the box 'Custom host'.
    Host: jabber.autistici.org
    Port: 5222
    TLS/SSL: Require TLS
