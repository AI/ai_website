title: Come configurare Tor Messenger
----

Come configurare Tor Messenger
==============================


Tor Messenger è una programma per chat orientato alla sicurezza, all'anonimato
e all'usabilità. Sviluppato da [Tor
Project](https://blog.torproject.org/blog/tor-messenger-beta-chat-over-tor-easily),
cerca di garantire un alto livello di usabilità accoppiandolo a un livello di
paranoia piuttosto elevato per quanto riguarda la riservatezza delle nostre
conversazioni online.

Troviamo questo software interessante per diversi motivi.
Innanzi tutto, **Tor Messenger instrada automaticamente le conversazioni
dell'utente verso la rete anonima Tor**, senza che sia necessario apportare
alcuna particolare modifica alla sua configurazione. Inoltre, le sessioni di
chat sono **cifrate di default** con il protocollo
[OTR](https://otr.cypherpunks.ca/). La sua **interfaccia
grafica** è pulita, inuitiva e disponibile in numerose lingue. In una parola:
**utilizzabile**.  Altro importante elemento di questo client per l'instant
messaging è la sua **interoperabilità**: potete installarlo e utilizzarlo su
Linux, Mac OS X e Windows. _Last but not least_, Tor Messenger è rilasciato
sotto licenza **GPL3**: il suo codice sorgente è pertanto liberamente
scaricabile, modificabile e liberamente ispezionabile.

Tor Messenger supporta diversi protocolli di chat, tra cui anche XMPP, lo stesso
utilizzato da Autistici/Inventati. Vediamo come configurarlo in pochi passaggi

## Connettersi direttamente alla rete Tor ##

La prima volta che lancerete Tor Messenger vi apparirà questa finestra:

![](/static/img/man_jabber/tormessenger/tm_1.jpg)

Cliccate sul pulsante _"Connect"_ e attendete qualche secondo.

![](/static/img/man_jabber/tormessenger/tm_2.jpg)

Una volta che il client avrà creato un circuito attraverso la rete Tor, vi
verrà chiesto di configurare il vostro account XMPP sui server di
Autistici/Inventati.


## Connettersi alla rete Tor mediante pluggable transport o proxy ##

Nella maggior parte dei casi non dovreste incontrare alcun problema nel
connettervi direttamente alla rete Tor. Se invece (a) il vostro provider blocca
l'accesso a Tor, (b) non volete che il vostro provider sappia che state
utilizzando Tor o (c) Usate un network - magari quello aziendale - che permette
l'accesso a Internet solo mediante un proxy, cliccate su _"Configure"_. 

Vi apparirà la seguente finestra:

![](/static/img/man_jabber/tormessenger/tm_3.jpg)

Se il vostro provider impedisce l'accesso alla rete Tor selezionate _"Yes"_ e
configurate un pluggable transport per aggirare  il blocco (vi rimandiamo alla
[documentazione ufficiale di Tor
Project](https://www.torproject.org/docs/pluggable-transports.html.en) per
ulteriori informazioni). Se invece non credete che la vostra connessione sia
censurata selezionate _"No"_ e passate alla schermata successiva.

![](/static/img/man_jabber/tormessenger/tm_4.jpg)

In questa seconda finestra vi viene chiesto se utilizzate un proxy per
connettervi a Internet. Se questo è il vostro caso, selezionate _"Yes"_ e
inserite gli appositi parametri di connessione. Non sapete quali sono?
Rivolgetevi alla vostra amministratrice (o amministratore) di rete per farvi dare
una mano. Se invece la vostra connessione di rete non usa un proxy, cliccate su
_"Continue"_. 


## Usare Tor Messenger con AI ##

Una volta connessi alla rete Tor, vi si aprirà un menu con i diversi protocolli
di comunicazione supportati da Tor Messenger. Il nostro server per chat utilizza
_"XMPP"_: selezionatelo e cliccate _"Next"_

![](/static/img/man_jabber/tormessenger/tm_5.jpg)

A questo punto inserite le credenziali del vostro account Autistici/Inventati.
Se il vostro account è bob@autistici.org, dovrete scrivere bob nella casella
_"Username"_ e autistici.org in _"Domain"_. Infine cliccate _"Next"_.

![](/static/img/man_jabber/tormessenger/tm_6.jpg)

Ora inserite la password del vostro account (o una password specifica per Jabber
se avete attivato la [Two Factor Autenthication](/docs/2FA) sui nostri
servizi) e cliccate _"Next"_.

![](/static/img/man_jabber/tormessenger/tm_7.jpg)

Quella che vi appare ora è la finestra delle _"opzioni avanzate"_. Non inserite
nessun paramentro e  proseguite cliccando il tasto _"Next"_

![](/static/img/man_jabber/tormessenger/tm_7_bis.jpg)

Tor Messenger è pronto per essere usato con i servizi chat di
Autistici/Inventati. Cliccate su _"Connect"_ e attendete alcuni istanti.

![](/static/img/man_jabber/tormessenger/tm_8.jpg)

Et voilà! Il gioco è fatto.

![](/static/img/man_jabber/tormessenger/tm_9.jpg)


## Usare Tor Messenger con l'hidden service di AI ##

Se volete garantirvi un livello ulteriore di privacy e sicurezza potete accedere
ai nostri servizi di chat utilizzando il nostro [hidden service
Tor](/docs/anon/tor). Per farlo, seguite la stessa configurazione che abbiamo
illustrato nel paragrafo precedente ("Usare Tor Messenger su AI") fino alla
finestra delle _"opzioni avanzate"_. Qui aprite il menu _"XMPP Options"_, e alla
voce _"Server"_ inserite l'indirizzo dell'hidden service di Autistici
(autinv5q6en4gpf4.onion).

![](/static/img/man_jabber/tormessenger/tm_10.jpg)


## Usare OTR ##

Come già spiegato nell'introduzione di questo tutorial, Tor Messenger cifrerà
in maniera automatica il contenuto delle vostre conversazioni con
[OTR](https://otr.cypherpunks.ca/) e non
permetterà l'invio di messaggi in chiaro: questo significa che la vostra
interlocutrice dovrà necessariamente usare un client XMPP che supporti a sua
volta OTR.

![](/static/img/man_jabber/tormessenger/tm_11.jpg)

**Non dimenticate di verificare l'autenticità della chiave!**

## Per approfondire ##

*   Tor Messenger [sul wiki di Tor
    Project](https://trac.torproject.org/projects/tor/wiki/doc/TorMessenger).
*   Tor Messenger [sul blog di Tor
    Project](https://blog.torproject.org/category/tags/chat-client).
*   [Il documento di
    design](https://trac.torproject.org/projects/tor/wiki/doc/TorMessenger/DesignDoc)
    di Tor Messenger.
