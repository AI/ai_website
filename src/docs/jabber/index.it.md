title: Guida a Jabber
----

Guida a Jabber
==============

Jabber (oggi meglio noto come XMPP) è un protocollo di instant messaging, ed è automaticamente disponibile a tutti gli utenti di A/I.

Ecco alcune delle sue caratteristiche:

- Nessuna registrazione delle informazioni di chi lo usa sui nostri server
- Connessioni crittate tra gli utenti del network di A/I
- Supporta [OTR](https://otr.cypherpunks.ca/) e [OMEMO](https://en.wikipedia.org/wiki/OMEMO) per la crittografia end-to-end

Parametri generici per configurare il client jabber
---------------------------------------------------

Il vostro utente Jabber ha esattamente lo stesso nome del
vostro indirizzo di posta su A/I.

Con un client XMPP sufficientemente moderno non c'è bisogno
di ulteriore configurazione, i meccanismi di autodiscovery
diranno al client a quale server connettersi.

Che programma scaricare per usare jabber?
-----------------------------------------

Un breve elenco di programmi che potete usare:

- [Pidgin](/docs/jabber/pidgin) (windows e linux) e [il suo plugin per la crittografia OTR](/docs/jabber/otr).
- [Conversations](/docs/jabber/conversations) (android)
- [Tor Messenger](/docs/jabber/tor_messenger) (windows, mac e linux)
- [CoyIM](/docs/jabber/coyim) (windows, mac e linux)
- [Adium](/docs/jabber/adium) (mac)
- [Psi](/docs/jabber/psi) (windows, mac e Linux; solo screenshot)
- [Xabber](/docs/jabber/xabber) (android)

Aiuto
-----

Il servizio è attivo a partire dal 25 Dicembre 2005. Per eventuali problemi, suggerimenti, o contributi nella relizzazione di una
documentazione efficace, contattateci attraverso [l'helpdesk](https://helpdesk.autistici.org).

Sull'implementazione
--------------------

Il servizio è implementato usando [Ejabberd](https://www.ejabberd.im/). Ringraziamo sentitamente il suo team di sviluppo.
