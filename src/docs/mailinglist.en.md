title: Mailing List Howto
----

Mailing list howto
==================

What is a mailing list - Public and private lists
-------------------------------------------------

A **mailing list** is essentially a list of mail addresses to which mails are sent automatically by a software installed on the server.

Depending on the configuration it's possible to define lists as **closed** if only subscribed users can send messages to it, or **open** if
anyone can send mail to it. Note that in any case mail sent to the list will be received only by subscribed users.

A list can be defined as **public** if its messages are archived on a public repository (such as [our public list web archive](http://lists.autistici.org))
or as **private** if only its subscribers can read archived messages.

Please keep in mind that if a list is **public** **messages sent to the list will appear sooner or later on Google and other search
engines**. Be aware of this downside of pulic list when you write a mail to it.

To participate to a list and receive messages sent to it you need to subscribe, but **public** lists allow you to read their archives online
and **open** lists allow you to write to a list without being subscribed to it.

To subscribe to a list you can visit our [hosted lists](/services/hosted/lists) page and find the **public** list you
want to become part of: click on it and fill the form with your e-mail address. You'll receive a confirmation link through an e-mail message
and once you'll have confirmed your identity, you'll be officially subscribed.

If the list is **private** the list administrator will have either to directly subscribe you or to send you a link to the subscription form
you'll have to fill.

Mailing list are very useful tools to communicate, to coordinate projects involving people very far away from each other and to debate
important issues if used correctly. It's difficult to define general rules to achieve proper use of mailing lists: every list has its own
customs and guidelines and the best idea is to ask those already part of the list some advice.

Now subscribe, read, write, enjoy!

Howto manage a mailing list
---------------------------

When you ask for a mailing list on our servers the first thing you'll have to do is log into the mailing list administration panel and
configure its various options: we have set the default options according to how *we think* people will want to use a mailing list, but our
ideas could be **very wrong** about your needs and wishes.

So log into your **https://www.autistici.org/mailman/admin/LISTNAME** page and start working on it.

### General Options

In the *General Options* page you will be able to set your list welcome page, its description, the welcome message for people subscribing to
it and many more basic stuff.

Most of the default options will be fine, but you should know that you shall **never change the *host\_name* option** since it would break
your list.

### Password

In the *Password* page you will be able to change and reset your admin password. You can also setup a moderator password to give away to
other users: a moderator is someone who will be able to approve or reject non-subscribers messages to the list, but not to change other list
settings.

### Membership Management

The *Membership management* page will easily be one of the most visited page in your list administrator experience.
 In the *Membership List* subpage you'll find a list of all the mail subscribed to the list along with a series of option for each one of
them:

- *mod* refers to moderated users, ie users whose message will need administrator approval before being forwarded to the list
- *nomail* refers to users who can still send mail to the list but who are not receiving the list messages for several reasons (their
    mailbox could be full or disabled, or they simply could be users interested only in sending messages but not in receiving them)
- *digest* and *plain* refer to the way these users receive the list messages: *plain* means they will get each mail individually, while
    *digest* allows them to receive a daily/weekly/monthly summary of the list messages in a single mail

If you want to unsubscribe a single user, just click on the *unsub* box next to his/her mail and then on *"Submit your changes"*.

To subscribe multiple users simultaneously, go to the *Mass subscription* subpage and fill in the form with one mail per line. You will also
be able to write a short welcome message to the ones you are subscribing.

### Privacy Options

The *Privacy Options* page is **one of the most important part** of your administration panel. Let's start with the *Subscription rules*.

- *Advertised* is the option to point out wether you want your list to be known to the public or not. If you say yes to this option your
    list will be included in our [hosted lists](/services/hosted/lists) page. The default value is **no**.
- *Subscribe Policy* specifies what a user should do to subscribe to a list: reply to an automatic mail sent by the mailing list software,
    wait for administrator approval or both. The default value **from april 2012** on is to require both steps to subscribe to the list, but
    before that date the default value was to allow subscription with a single mail reply, so better check what is your list situation if
    you are a A/I old timer.
- The *Ban list* is a list of email who are forbidden subscription to the list
- The last interesting option allows you to choose who can see the list of people subscribed to the list. The default option allows only
    list subscribers to see who are the other people subscribed, but it's possible to extend this possibility to anyone or to shrink it to
    the administrator only.

The *Sender filters* subpage will let you decide what to do with messages coming from non-subscribers by default: you can decide a list of
mail address whose messages are automatically *accepted*, *rejected* or *discarded*.

It's this subpage where a list administrator can set the options needed to have an **open list** where everybody can send emails without
the need for the administrator approval.

### Archiving Options

The *Archiving Options* page is **very important** since it includes the options to decide wether a list messages will be **public** or
**private**. Please be careful with this page options since setting up a **public list** implies that your subscribers messages will be
available on search engines freely!

- The *Archive* option allows the administrator to decide wether to keep a copy of the list messages on the servers or not. The default
  option is **no** so if you do not change this the only copy of the list mails will be in your subscribers computers.
- The *Archive private* option allows the administrator to decide wether the list archive will be publicly available on [our servers](http://lists.autistici.org/)
  or only available to subscribers on a dedicated page (whose link you will fine in your
  administration panel)

### Tend to pending moderator requests

The *pending requests* page allows administrators and moderators to choose what to do with non-subscribers messages. For each message you
will be able to decide wether to forward it to the list or not, and what to do with further messages from the same mail address.

- *Defer* means your are postponing your decision: the message will be hold furtherly on the page
- *Accept* means you want to forward the message to the list
- *Reject* means you don't want to forward the message to the list and that you want to send a message to the author notifying the rejection
- *Discard* means you don't want to forward the message to the list and that you don't want to notify the rejection to the author
