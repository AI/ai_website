title: IRC Services Howto
----

IRC Services Howto
==================

## Summary

- [introduction](#intro)
- [how to use services](#comesiusa)
- [how to use ChanServ](#chanserv)
- [how to manage takeovers](#takeover)

- - -

<a name="intro"></a>

## Introduction

Services are applications running on the IRC server and allowing to manage nicknames and channels in a clear and effective way. Currently
available services are:

- **NickServ:** This is a nickname manager that users can use to protect themselves from nickname abuses.
- **ChanServ:** A channel manager helping users to manage their channels in a highly customizable way. ChanServ keeps a list of privileged
    and banned users for each channel and it thus allows for access control. It also overcome all takeover problems thanks to
    op/unban/invite functions, as well as a function of mass deop and mass kick.

- - -

<a name="comesiusa"></a>

## How to use services

Services are programs that can be used by sending a message to virtual users NickServ and ChanServ. Thus, supposing you want to
ask help about NickServ commands, you'll just write:

    /msg NickServ HELP

this command should work with all clients. Some clients allow to cut the command short by writing:

    /nickserv HELP
    /ns HELP

this doesn't work with all clients (BitchX for instance). The corresponding commands for ChanServ are:

    /msg ChanServ HELP
    /chanserv HELP
    /cs HELP

**<u>How to use NickServ</u>**

The first thing you should do when you use IRC Services is registering your nickname. You'll thus prevent someonelse from abusing your nick.
Besides, you can only use other services if your nickname is registered and recognized. In order to register a nickname, use the REGISTER
command:

    /msg NickServ REGISTER mypassword mymail@address.com

you don't need to enter your real e-mail address, but if you don't you will not be able to recover your password if you lose it. NickServ
will respond to this command with the answer:

    -NickServ- Nickname pho registered under your account

thus notifying that your nickname has been properly registered. When you've registered, every time you reconnect to the IRC server you'll
get the following message from NickServ:

    -NickServ- This nickname is registered and protected. If it is your
    -NickServ- nick, type /msg NickServ IDENTIFY password. Otherwise,
    -NickServ- please choose a different nick.

To answer this message, type:

    /msg NickServ IDENTIFY mypassword
    -NickServ- Password accepted - you are now recognized.

You can check that an user has identified with her nick through the command WHOIS. What follows is for instance the result of a 'whois'
about an unidentified user (note that 'identified' is different from 'registered', as a nick can be registered but someone could connect to
IRC with someone else's nick and ignore the password request...).

    /whois megabug

    .----------------------------------------- -- -
     | megabug (~megabug@29773c92.1959841d.dialup.tiscali.it) (Italy)
     : ircname : megabug
     | server : paranoia.irc.mufhd0.net (A/I IRC Server)
     : idle : 0 hours 4 mins 17 secs (signon: Tue Apr 23 15:42:01 2002)

if an user has identified, a WHOIS about her will be as follows:

    /whois megabug

    .----------------------------------------- -- -
     | megabug (~megabug@29773c92.1959841d.dialup.tiscali.it) (Italy)
     : ircname : megabug
     | register : megabug - is a registered nick
     | server : paranoia.irc.mufhd0.net (A/I IRC Server)
     : idle : 0 hours 4 mins 17 secs (signon: Tue Apr 23 15:42:01 2002)

note that now the line "register : megabug - is a registered nick" has appeared.

If someone willingly 'steals' your nick, you can force her to abandon it with the RECOVER command:

    /msg NickServ RECOVER megabug mypassword

    -:- megabug is now known as Guest9568776
    -NickServ- User claiming your nick has been killed.
    -NickServ- /msg NickServ RELEASE megabug to get it back before the one-minute timeout.

the user abusing your nick is thus 'renamed' by force. IRC Services will now keep your nick for one minute, after which you can recover it:

    /nick megabug

    &gt;&gt;&gt; You(megabug\_) are now known as megabug
    -NickServ- This nickname is registered and protected. If it is your
    -NickServ- nick, type /msg NickServ IDENTIFY password. Otherwise,
    -NickServ- please choose a different nick.

    /msg NickServ IDENTIFY mypassword

    -NickServ- Password accepted - you are now recognized.

We can even keep our nick protected when we are not connected, so that if you don't identify within one minute your nick is automatically
recovered. This is one of the options we can activate for our nick with the SET command. In order to obtain the complete options list for
this command, just enter:

    /msg NickServ HELP SET

the option we were just talking about is KILL -- if you want to activate it, write:

    /msg NickServ SET KILL ON

    -NickServ- Protection is now ON.

moreover, if you connect without a static IP, you should delete all entries in your access list (for further information on this feature,
read the NickServ online help). However, the commands you need to clean up your access list are as follows:

    /msg NickServ ACCESS LIST

    -NickServ- Access list:
    -NickServ- \*megabug@\*.2d34f077.stru.polimi.it

this command shows your access list. Delete the entry "\*megabug@\*.2d34f077.stru.polimi.it" with command:

    /msg NickServ ACCESS DEL \*megabug@\*.2d34f077.stru.polimi.it

    -NickServ- \*megabug@\*.2d34f077.stru.polimi.it deleted from your access list.

    /msg NickServ ACCESS LIST

    -NickServ- Access list:

your access list will now be empty, If you try reconnecting to the IRC server, you'll read the following message:

    -NickServ- This nickname is registered and protected. If it is your
    -NickServ- nick, type /msg NickServ IDENTIFY password. Otherwise,
    -NickServ- please choose a different nick.
    -NickServ- If you do not change within one minute, I will change your nick.

(f you don't change within one minute)

    &gt;&gt;&gt; You(megabug) are now known as Guest9568777
    -NickServ- Your nickname is now being changed to Guest9568777

NickServ has thus forced the nick change you wanted.

- - -

<a name="chanserv"></a>

## How to use ChanServ

After you've registered your nickname, you can use it to register a channel through ChanServ.

In order to register a channel, you naturally have to be Op(erator) of this channel, and the channel mustn't be already registered or
currently used by others.

    /join \#test

    -:- megabug \[~megabug@1e79b4c9.27ea28c2.fastres.net\] has joined \#test
    -:- \[Users(\#test:1)\]
    \[@megabug \]
    -:- Channel \#test was created at Sat Apr 27 02:21:49 2002

Now you are an operator of the \#test channel, and since \#test hasn't been registered yet, you can register it with the REGISTER command:

    /msg ChanServ REGISTER \#test mypassword Channel description

    -ChanServ- Channel \#test registered under your nickname: megabug
    -ChanServ- Your channel password is mypassword - remember it for later use.

Your channel has been registered and the registering user has become channel founder. A founder can change access modality for her channel,
can define a list of nicks enjoying particular privileges, etc.

Now let's try to exit and re-enter the channel.

    /part #test
    /join #test

    -:- megabug \[~megabug@1e79b4c9.27ea28c2.fastres.net\] has joined \#test
    -:- \[Users(\#test:1)\]
    \[@megabug \]
    -:- mode/\#test \[+nrt\] by ChanServ
    -:- Topic (\#test): changed by ChanServ: (ChanServ)
    -:- mode/\#test \[+o megabug\] by ChanServ
    -:- Channel \#test was created at Sat Apr 27 02:34:31 2002

as you can see, Chanserv has automatically opped you. Auto-Op is a very nice feature of ChanServ. A founder is opped by default every time
she accesses her channel (also because she has the highest privileges in the channel). Furthermore, a founder can write a list of nicks
which are automatically given operator status when they connect to the channel.

Actually a founder can keep three different privileges lists:

- **VOP** (VOice People) - This list contains nicks which will automatically receive Voice modality when they access the channel.
- **AOP** (Auto OP) - This list contains nicks which will automatically receive Op status when they access the channel.
- **SOP** (Super OP) - People included in this list have the same privileges as AOPs, but can also use AutoKick and read or write messages
    in the channel memo.

These three lists use the same commands for adding/removing nicks, so a command for AOP list such as:
    
    /msg ChanServ AOP \#test LIST

has a corresponding line for VOP and SOP lists:

    /msg ChanServ VOP \#test LIST
    /msg ChanServ SOP \#test LIST

this applies to all commands described below.

The LIST command we have just seen shows the content of each list. Since the channel has just been created, the list is obviously empty.

    -ChanServ- \#test AOP list is empty.

If you want to add a nick to the list, use the ADD command:

    /msg ChanServ AOP \#test ADD baku

    -ChanServ- baku added to \#test AOP list.

    /msg ChanServ AOP \#test LIST

    -ChanServ- AOP list for \#test:
    -ChanServ- Num Nick
    -ChanServ- 1 baku

User baku has thus been added to the AOP list in the channel.

Similarly, you can delete this nick with the DEL command

    /msg ChanServ AOP \#test DEL baku

    -ChanServ- baku deleted from \#test AOP list.

These few commands help manage channel access. Only channel founders can modify these lists. It can also be useful to acquire channel
privileges even if you connect to the IRC server with a different nick. To do this, you can use the IDENTIFY command:

    /msg ChanServ IDENTIFY \#test miapassword

    -ChanServ- Password accepted - you now have founder-level access to \#test.

In this case, even if you have not materially founded a channel, you can obtain founder privileges just by knowing the channel password; or
you can give the channel password to trusted people who can help us manage the channel.

<a name="takeover"></a>

## How to manage takeovers

If you have founder privileges for a channel, you shouldn't worry much about takeovers, as you can use a recovery system even for extreme
situations. The command you need, and which of course only founders can use, is the CLEAR command. This command has some options you can use
depending on the situation:

    /msg ChanServ CLEAR \#test OPS

This command deops all \#test users. It can be useful if someone takes over the channel by deopping everybody without kicking/banning them.

    /msg ChanServ CLEAR \#test BANS

If someone takes over the channel and then kicks and bans all users, you can remove the ban with this command. It must naturally be used
after a CLEAR OPS so that the person taking over the channel cannot ban again soon after.

    /msg ChanServ CLEAR \#test MODES

This command removes all channel modes (in particular +k +i and +l). If someone sets an invite-only, a password-protected (+k) or a
user-limit (+l) mode on your channel, you can reset the configuration with this command. For the reason above, you should of course make a
CLEAR OPS before giving this command.

    /msg ChanServ CLEAR \#test USER

This command removes all users from te channel, in practice with a mass kick...
It should be clearly avoided, but in some cases this is the easiest solution.

Once you get your channel under control, you can obtain operator status (if ChanServ hasn't given them automatically) by entering the
command:

    /msg ChanServ OP
