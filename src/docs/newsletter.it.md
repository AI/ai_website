title: Come si usano le newsletter
----

Come si usano le newsletter
==================

Che cos'è una newsletter
-------------------------------------------------------------

Una newsletter è sostanzialmente una lista di indirizzi a cui automaticamente vengono inviati dei messaggi da un programma presente sul
server (la macchina che fornisce il servizio).

A differenza di una mailing list, le newsletter sono impostate per non ricevere le eventuali risposte degli iscritti, 
in pratica funzionano come una mailing list **chiusa**.

Per partecipare e ricevere i messaggi di una certa newsletter è necessario iscriversi

Per iscriversi dovete conoscere il nome della newsletter e visitare la pagina **https://noise.autistici.org/mailman/listinfo/NOME**
completando un semplice form vi verrà recapitata nella casella di posta una richiesta di conferma 
(onde evitare che qualcuno iscriva indirizzi non propri) e una volta risposto ad essa comincerete
a ricevere i messaggi di quella newsletter.

Enjoy!

Amministrare una newsletter
-----------------------------

Quando aprite una newsletter sui nostri server (gestita dal software "Mailman"), la prima cosa che dovrete fare è collegarvi 
al pannello di amministrazione e configurare le opzioni: noi abbiamo impostato alcuni valori predefiniti, e alcuni di questi 
non vanno modificati:

    send_reminders = 0                                                                                                                                
    respond_to_post_requests = 0                                                                                                                      
    default_member_moderation = 1                                                                                                                     
    generic_nonmember_action = 3                                                                                                                      

se lo fate torneranno al valore precedente entro pochi minuti.
Per il resto collegatevi alla pagina **https://noise.autistici.org/mailman/admin/NOME** e cominciate a spulciare le opzioni.

### Opzioni Generali

Nella sezione *Opzioni generali* troverete le impostazioni che riguardano la pagina di benvenuto della lista, la sua descrizione, i messaggi
di benvenuto da inviare agli utenti, la grandezza massima degli allegati che possono essere accettati, ecc.

La maggior parte dei parametri che trovate predefiniti in questa sezione dovrebbero andare bene (ovviamente le descrizioni e i messaggi di
benvenuto sono a vostra totale discrezione e non hanno valore preimpostato), ma **non dovrete mai cambiare l'opzione "Nome host
(host\_name)"** altrimenti la vostra newsletter smetterà di funzionare.

### Password

Questa sezione serve ovviamente a reimpostare la password di amministrazione.

### Gestioni iscritti

Non c'è bisogno di dirvi che questa sarà una delle sezioni delle opzioni di impostazione che frequenterete con maggiore assiduità.
 Nell'*Elenco iscritti* troverete un elenco (appunto) di tutte le persone iscritte alla lista seguite da una serie di opzioni:

- *Moderato* significa che l'iscritto non potrà scrivere all'indirizzo della newsletter (e deve essere cosi', altrimenti diventa una
	mailing list). Solo chi e' autorizzato a spedire i messaggi deve essere non moderato, quindi ad esempio dovete mettere come 
	non moderato il vostro indirizzo e/o quello di chi e' autorizzato a inviare i messaggi.
- *Nomail* significa che l'iscritto - per diversi motivi - non sta ricevendo le mail della lista (es. la sua mailbox potrebbe essere
    non funzionante).
- *Digest* e *Testo* indicano con quale modalità l'utente riceve i messaggi della lista: messaggio per messaggio (*Testo*) o in una sola
    mail in cui vengono raccolti i messaggi di un giorno, di una settimana o di un mese (*Digest*) (sconsigliato per una newsletter)

Se volete disiscrivere un singolo utente, cliccate sul box *Cancella* a fianco della sua mail e poi su *"Applica le modifiche"* in fondo
alla pagina.

Per iscrivere invece molti indirizzi tutti insieme potete visitare la sezione *Iscrizione di massa* dove potrete includere - uno per riga -
un elenco di mail da aggiungere alla lista e un eventuale messaggio di benvenuto personalizzato.

### Opzioni per la privacy

Questa è **una delle sezioni più importanti**. Cominciamo con le *Regole di iscrizione*.

- *Advertised* è l'opzione che indica ai nostri server se la lista deve essere inserita nell'elenco delle liste disponibili sui nostri
    server: ciò significa che l'esistenza della lista sarà **nota al pubblico** inclusa la sua descrizione. Il valore predefinito è **no** e
    così dovrebbe restare se la lista è uno strumento di dibattito interno a un gruppo, collettivo, e via dicendo.
- *Policy di iscrizione* è l'opzione che specifica quali passaggi un utente deve compiere per iscriversi, con complicazioni crescenti: se
    basta rispondere a una mail automatica mandata dal server per essere iscritti; se è necessario attendere l'approvazione
    dell'amministratore prima di essere iscritti; se sono previsti entrambi i passaggi. Il valore predefinito è
    che sono necessari entrambi i passaggi. 
- *Policy di disicrizione* è l'opzione che indica se per disiscriversi è necessaria l'approvazione del moderatore. Viviamo in un mondo
    libero quindi il valore predefinito è **no**
- *Banlist* è l'elenco delle mail a cui è vietato iscriversi alla lista
- L'ultima opzione interessante è quella che definisce a chi è consentito vedere gli iscritti alla lista: normalmente l'operazione è
    consentita ai soli iscritti, ma potete decidere di rendere pubblico l'elenco oppure di permettere al solo amministratore
    questa sbirciata.

Nei *Filtri sul mittente* è impostato che i messaggi dei non iscritti devono essere *scartati* (ovvero cestinati senza che la cosa 
 venga notificata agli autori).
Se volete autorizzare all'invio qualcuno che non e' iscritto alla newsletter,
andate nella sezione Privacy -> Filtri sul mittente                                                                         
e cercate l'opzione "List of non-member addrresses whose postings should be automatically accepted" 
e aggiungete l'indirizzo da autorizzare.                                                                                                                                              


### Opzioni di archiviazione

Questa sezione delle opzioni di configurazione della lista **molto importante** è quella in cui l'amministratore decide se una lista avrà
archivi **pubblici** o **privati**. Vi preghiamo di prestare attenzione perché una **lista pubblica** implica che i messaggi degli iscritti
e i loro dati vengano indicizzati dai motori di ricerca!

- *Archivi* definisce se la lista conservi sui nostri server i messaggi inviati o meno. Il valore è predefinito è **no** e quindi i
    messaggi delle vostre liste *non* saranno conservati se non nelle mailbox degli iscritti
- *Pubblici o Privati* è l'opzione che definisce se gli archivi (ove presenti) della lista saranno a disposizione di tutto il pubblico sui
    [nostri server](http://lists.autistici.org/) o solo in una pagina dedicata (il cui link trovate nel pannello di amministrazione
    della lista).



Per i dettagli vi rimandiamo al manuale di mailman:

**https://www.gnu.org/software/mailman/mailman-admin/index.html**       
