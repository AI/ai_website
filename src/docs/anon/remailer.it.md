title: Anonymous Remailer - guida
----

Anonymous Remailer - guida
===========================

Qui trovate le istruzioni base in italiano per capire come vengono costruiti i messaggi per il remailer, se usate un client specifico non
e' necessario imparare questo procedimento, pero' aiuta a capire come funzionano i remailer.

Se volete mandare un messaggio anonimo, prima create un file che contenga:

- nella prima riga due segni di "due punti",
- nella seconda riga la scritta "Anon-To: indirizzo-email "

dove indirizzo e-mail e' l'indirizzo a cui il remailer mandera' il messaggio.

La terza riga deve essere vuota, dopodiche' ci sara' il testo del messaggio.

 es.:

 	============================================================= 
	:: 
	Anon-To: destinatario@esempio.org  

	Qui' c'e' il testo del messaggio 
	=============================================================

Il remailer accetta solo messaggi criptati con PGP o GPG, quindi il messaggio deve essere criptato con la chiave pubblica del remailer che
e' possibile ottenere mandando un messaggio al remailer (mixmaster@remailer.paranoici.org) che contenga nel subject "remailer-key".

Quindi il messaggio sopra va criptato con la chiave PGP del remailer, e infine spedito ad mixmaster@remailer.paranoici.org inserendo
all'inizio del messaggio due doppi punti e, nella seconda riga, la scritta "Encrypted: PGP", seguita dal messaggio criptato
precedentemente.

 	============================================================= 
	From: joe@test.com 
	To: mixmaster@remailer.paranoici.org  
	
	:: 
	Encrypted: PGP  

	-----BEGIN PGP MESSAGE----- 
	Version: 2.6.3i  
	owE1jMsNwjAUBH3gZMk9PClnUoBPUANpwElW2OBPZD8H0gd1UCP2gduuNDNfj
	IcSHT4zCbQmtlbzGFM9T0jSD7QVvEzaPcUlBSSWHQclbnR9YWJNp5BFSLdR9s 
	CijF3NGxybry/1Rsqn4la3a0JiIhLvnYGCu9HFtiC8oIxnlkeuIYe+EH=HgDa 
	-----END PGP MESSAGE----- 
	=============================================================

Il remailer decrittera' il messaggio e lo spedira' anonimamente. Se vuoi includere un Subject o altri header che non saranno filtrati dal
remailer puoi inserirli come indicato qui' sotto prima di criptare il messaggio per il remailer:

	============================================================= 
	:: 
	Anon-To: destinatario@esempio.org  
	## 
	Subject: Re: Twofish 
	In-Reply-To: Your message of "Tue, 12 Jan 1999 22:47:04 EST." <199901130247.WAA02761@example.com>  
	
	Testo del messaggio 
	=============================================================

Anche se la crittazione con PGP e' molto sicura, usare un remailer nel modo piu' semplice non e' il modo migliore per proteggere la propria
identita'. Per questo ad esempio si puo' dire al remailer di trattenere il messaggio che noi gli inviamo per un certo tempo, per reinviarlo
piu' tardi per evitare le cosiddette analisi del traffico.

Se inserisci l'header `Latent-Time: +2:00` il messaggio sara' ritardato di 2 ore, se invece usi la sintassi `Latent-Time: +5:00r` il
ritardo sara' random (ossia casuale) e compreso tra 0 e 5 ore.

Il modo migliore per utilizzare i remailer e' quello di usarli in catena, inviando un messaggio da un remailer all'altro e infine al
destinatario.

Ad esempio, se prendi il messaggio visto sopra da spedire al remailer:

	============================================================= 
	:: 
	Anon-To: mixmaster@remailer.paranoici.org  
	
	:: 
	Encrypted: PGP  
	
	-----BEGIN PGP MESSAGE----- 
	Version: 2.6.3i  
	owE1jMsNwjAUBH3gZMk9PClnUoBPUANpwElW2OBPZD8H0gd1UCP2gduuNDNfI 
	T4zCbQmtlbzGFM9T0jSD7QVvEzaPcUlBSSWHQclbnR9YWJNp5BFSLdR9CijF3 
	ybry/1Rsqn4la3a0JiIhLvnYGCu9HFtiC8oIxnlkeuIYe+EH=HgDq 
	-----END PGP MESSAGE----- 
	=============================================================

puoi criptarlo con la chiave PGP di un altro remailer (ad esempio Cripto) e mandare il messaggio cosi' criptato a questo remailer
[anon@ecn.org](mailto:anon@ecn.org)

In questo modo Cripto ricevera' il messaggio, lo decrittera' e trovera' le istruzioni per spedirlo ad mixmaster@remailer.paranoici.org, che
a sua volta potra' decrittarlo e spedirlo a destinatario@esempio.org.

Per una migliore spiegazione del meccanismo della concatenazione dei remailer visitate: <http://www.ecn.org/kriptonite>

Per l'utilizzo del mixmaster sotto linux e per un elenco di software crittografici: [autistici.org/crypto](https://autistici.org/crypto) e per altre
informazioni la pagina web del nostro remailer: <http://remailer.paranoici.org>



