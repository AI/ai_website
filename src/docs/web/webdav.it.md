title: Come aggiornare il tuo sito usando WebDAV
----

Come aggiornare il tuo sito usando WebDAV
===========================================================

Per accedere al proprio spazio web[\*](#*) su Autistici/Inventati, si usa un accesso mediante il protocollo
[WebDAV](https://it.wikipedia.org/wiki/Webdav) ("Web Distributed Authoring and Versioning", un estensione del protocollo
[HTTP](https://it.wikipedia.org/wiki/HTTP)). Fino a poco tempo fa l'accesso avveniva attraverso il protocollo
[FTP](https://it.wikipedia.org/wiki/FTP), ma ciò non si è più dimostrato pratico per garantire il miglior anonimato di chi aggiorna un sito
sui nostri server.

Le uniche informazioni che vi servono per collegarvi via WebDAV al vostro sito è il nome del **server**
(***https://www.autistici.org/dav/*** unico per tutti i nostri utenti), il vostro nome utente, che troverete nel vostro [Pannello
utente](https://accounts.autistici.org) sotto la sezione **Websites**, e la password che vi abbiamo inviato al momento dell'apertura del sito. E' importante che
il nome del server sia **sempre** `www.autistici.org`, oppure alcuni client potrebbero confondersi e non funzionare.

La cartella contenente il vostro sito è quella chiamata **html-*nomesito*** che troverete dopo esservi collegati via WebDAV al vostro
server.

Se volete **cambiare la vostra password WebDAV** potete farlo visitando direttamente il vostro [Pannello Utente](https://accounts.autistici.org): fate login
con il vostro utente per la mail e la vostra password della mail, cliccate sulla sezione **Websites** e cercate il link **"Cambia
password"**.

Per collegarsi a un sito usando il protocollo WebDAV è necessario usare un apposito programma client, differente a seconda del sistema
operativo che utilizzate.

Linux - Nautilus (interfaccia grafica)
--------------------------------------

Per collegarvi al vostro sito via WebDAV potete sfruttare il vostro filebrowser *Nautilus* (quello con cui guardate il contenuto delle
cartelle sul vostro pc).

Aprite una qualsiasi cartella o directory sul vostro pc.

![](/static/img/man_webdav/nautilus.png)

Portandovi con il mouse in alto a sinistra nel Menù *File* potrete selezionare la voce *Connect to Server...*. Il software vi presenterà una
maschera dove inserire alcuni dati per collegarvi al server remoto.

![](/static/img/man_webdav/nautilus-connect-form-empty.png)

Dal men\` a tendina selezionate la voce *"Secure WebDAV (HTTPS)"* e compilate i seguenti campi con i dati appropriati (il vostro nome utente
e la vostra password di accesso):

-   Server: www.autistici.org
-   Port: 443
-   Folder: /dav/nomeutente
-   User Name: nomeutente
-   Password: password

![](/static/img/man_webdav/nautilus-connect-form.png)

Cliccate sul pulsante *"Connect "*.

Il filebrowser vi presenterà il contenuto della vostra cartella sul server. Caricate il vostro sito nella directory *"html-nomesito"* e sarà
visibile online.

In basso a sinistra noterete una sezione *"Network"* dove potrete trovare un collegamento alla vostra directory remota fino a che non vi
scollegherete dal server.

Per scollegarvi dal server cliccate sul triangolino evidenziato dal cerchio blu nell'immagine. Buon lavoro!

![](/static/img/man_webdav/nautilus-remote-dir.png)

Linux - Cadaver (riga di comando)
---------------------------------

Per i sistemi GNU/Linux il client più diffuso per collegarsi a un sito via WebDAV è [**cadaver**](http://www.webdav.org/cadaver/), che
purtroppo funziona solo a riga di comando: scaricate il pacchetto e installatelo.
 Per collegarsi in un terminale digitate il seguente comando:

    $ cadaver https://www.autistici.org/dav/nomeaccountweb

Accettate il certificato e inserite nome utente e password per l'accesso web (che trovate nel vostro [pannello utente](https://accounts.autistici.org))

L'interfaccia di *cadaver* é a riga di comando. I comandi che verosimilmente avrete bisogno di usare sono i seguenti:

-   *cd*: per spostarvi nelle diverse cartelle o directory sul server
-   *lcd*: per spostarvi nelle diverse cartelle o directory sul vostro pc
-   *ls*: per vedere il contenuto di una cartella o directory sul server
-   *lls*: per vedere il contenuto di una cartella o directory sul vostro pc
-   *put*: per caricare un file dal vostro pc sul server
-   *mput*: per caricare più file o directory (aggiungete -r) dal vostro pc sul server
-   *get*: per scaricare un file dal server sul vostro pc
-   *mget*: per scaricare più file o directory (aggiungete -r) dal server sul vostro pc

davfs2 (riga di comando automatizzabile)
----------------------------------------

Tutti gli utenti Linux oltre ad aver la possibilità di collegarsi tramite Nautilus (e Thunar) e con Cadaver hanno anche la possibilità di
usare davfs2 per montare il sito su una directory a loro scelta.

Prima di tutto installatelo, per le Debian based basterà un:

    # apt-get install davfs2

A questo punto aggiungete l'utente a cui volete dare i permessi per montare/smontare il sito al gruppo 'davfs2':

    # gpasswd -a nomeutente davfs2

Create quindi la directory dove volete montare il sito:

    # mkdir /media/nomesito

E date quindi i permessi al vostro utente:

    # chown nomeutente /media/nomesito
    # chmod 0700 -R /media/nomesito

Ora non vi resta che modificare l'fstab per dare la possibilità al vostro utente di montare e smontare la directory a suo piacimento:

    # nano /etc/fstab

    ### aggiungete questa linea in fondo al vostro fstab, state attenti a non far danni.
    https://www.autistici.org/dav/nomesito /media/nomesito davfs noauto,users,rw,uid=nomeutente 0 0

A questo punto, se volete fare il mount usando il vostro utente dovete dare i permessi setuid all'eseguibile,

    # chmod u+s /sbin/mount.davfs

che pero' non e' il massimo per la sicurezza, l'alternativa e' che facciate il mount come utente root o usando sudo.

Bene a questo punto provate a vedere se funziona dando il comando:

    $ mount /media/nomesito

Se ricevete l'errore "/sbin/mount.davfs2 file /home/nomeutente/.davfs2/secrets has wrong permissions" assicuratevi che il file
~/.davfs2/secrets abbia come permessi 0600:

    $ chmod 0600 ~/.davfs2/secrets

Se volete inoltre evitare che vi chieda ogni volta l'username (e volendo anche la password, anche se e' sconsigliato se non avete una
partizione crittata) potete modificare il file secrets in questo modo:

    $ nano ~/.davfs2/secrets

    /mount/nomesito nomeutente password \#la password potete anche non metterla se volete farla chiedere ogni volta che date il comando.

Quando vorrete smontare il sito semplicemente usate: $ umount /media/nomesito Volendo potete anche impostare l'auto mount all'avvio,
modificando il parametro nell'fstab da 'noauto' a 'auto' e specificando la password nel file secrets

MAC OSX
-------

Dal menu del Finder/Scrivania selezionate "Go/Connect to server" (Vai/Collegati al server) e vi comparira' la finestra dove inserirete
questi dati:

![](/static/img/man_webdav/2_webdav_connecttoserver.jpg)

Il *Server address* deve essere *https://www.autistici.org/dav/NOMEUTENTEWEBDAV* in cui dovrete sostituire a *NOMEUTENTEWEBDAV* il vostro
nome utente.

Poi cliccate sul segno piu' *(+)* per aggiungere questo indirizzo ai preferiti e quindi sul pulsante *"Connect" (Connetti)*.

![](/static/img/man_webdav/3_webdav_userpass.jpg)

Inserite username e password (vedi sopra) e avrete a disposizione una sorta di disco dove copiare o dal quale scaricare file. Per
disconnettervi fate come fareste con un qualsiasi disco rigido o chiavetta USB, per esempio cliccate sull'icona a forma di triangolo accanto
al nome del server.

Windows / Mac
-------------

Oltre a usare il tremebondo Internet Explorer potete scaricare un client WebDAV specifico come **[CyberDuck](http://cyberduck.ch/)**.

Una volta installato il software, lanciate il programma.

![](/static/img/man_webdav/Cyberduck01.jpg)

1.  Cliccare su "Nuova connessione"
2.  Inserire le credenziali:
    -   Server: www.autistici.org
    -   Porta: 443
    -   Nome utente: nome dell'utente webdav
    -   Password: password dell'utente webdav
    -   Più opzioni - Percorso: /dav/NOMEUTENTEWEBDAV
3.  Cliccare su "Collegamento"
4.  Inserite nome utente e password

![](/static/img/man_webdav/Cyberduck02.jpg)

Se tutto è andato per il verso giusto compare la finestra con le cartelle/directory del vostro spazio web

![](/static/img/man_webdav/Cyberduck03.jpg)

iPAD
----

Potete collegarvi usando la App **"WebDAV Nav"** (gratuita ed a pagamento) che trovate sull'App Store.

1.  Lanciate la App.
2.  Cliccate sul pulsante con il segno piu' *(+)*
3.  Inserite i dati per la connessione:
    -   Nome: quello che volete, tanto serve solo per ricordarvi di cosa si tratta
    -   ServerURL: https://www.autistici.org/dav/NOMEUTENTEWEBDAV (in cui NOMEUTENTEWEBDAV sarà il nome utente che trovate nel vostro
        [pannello utente](https://accounts.autistici.or)
    -   Nome Utente: il nome che avete trovato nel vostro [pannello utente](https://accounts.autistici.org) (si, sempre quello)
    -   Password: inserite la vostra password di accesso webdav
4.  Cliccate sul pulsante "Salva".
5.  Nella finestra adesso vi compare il nome che avete scelto, cliccateci sopra e se vi compare una schermata con delle cartelle azzurre
    significa che vi siete collegati.


Gitlab CI
---

Se usate un'istanza Gitlab per generare un sito statico, probabilmente
vi conviene aggiornare automaticamente il sito ogni volta che modificate
la vostra repository. Per farlo, potete usare [webdav-upload](https://git.autistici.org/ai3/tools/webdav-upload), che vi permette di caricare
i file nel vostro spazio web attraverso la continuous integration di Gitlab.

Per farlo, dovrete aggiungere i seguenti comandi al vostro file `.gitlab-ci.yml`:

1. Per installare webdav-upload avrete bisogno di pip3, quindi se python3-pip non è installato di default nell'immagine Docker che usate nel vostro script `.gitlab-ci.yml`, per prima cosa dovrete installare questo pacchetto. Se per esempio state usando un'immagine Debian, il primo comando del vostro script dovrà essere:

          - apt-get update -y && apt-get install -yq python3-pip

1. A questo punto potete installare webdav_upload con il comando:

        pip3 install git+https://git.autistici.org/ai3/tools/webdav-upload#egg=webdav_upload
        
2. Infine, caricate i file del vostro sito statico nel vostro spazio web con il seguente comando:

        webdav-upload --user="$WEBDAV_USER" --password="$WEBDAV_PASSWORD" --url "https://www.autistici.org/dav/$WEBDAV_USER/" output-folder html-yourwebsite

    In questo comando, dovrete sostituire alcuni elementi:

    - `output-folder` va sostituito con il nome del folder in cui viene generato il sito nel Gitlab runner.
    - `html-yourwebsite` va sostituito con il nome della cartella nel vostro spazio web. Per esempio, se il vostro sito si trova all'indirizzo inventati.org/foo, il nome della cartella sarà `html-foo`.
    - Potete salvare il vostro nome utente e la vostra password come variabili nei setting della vostra CI (Settings -> CI/CD -> Variables). Nel comando riportato sopra, il nome delle due variabili è WEBDAV_USER per il nome utente e WEBDAV_PASSWORD per la password.

        **È importante usare questo metodo per il nome utente e la password, che non dovrebbero essere incluse nello script, soprattutto se la vostra repository è pubblica!** 

Di seguito, uno script di esempio:

```
production:
  stage: deploy
  script:
    - apt-get update -y && apt-get install -yq python3-pip
    - pip3 install git+https://git.autistici.org/ai3/tools/webdav-upload#egg=webdav_upload
    - jekyll build -d output-folder
    # qui stiamo immaginando che usiate jekyll per generare il vostro sito e
    # che salviate i file generati in una cartella chiamata `output-folder`,
    # ma potete generare il sito con qualunque sistema per generare siti statici
    - webdav-upload --user="$WEBDAV_USER" --password="$WEBDAV_PASSWORD" --url "https://www.autistici.org/dav/$WEBDAV_USER/" output-folder html-yourwebsite
```
