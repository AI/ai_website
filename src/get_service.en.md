title: Request a service
----

# Request a service

You want an email, a mailing list, a website hosted by A/I?
The reason is that your privacy matters a lot to you and you would like an
account that is disconnected from your official identity?

This is absolutely reasonable, but our motivations go beyond privacy and
anonymity -- Autistici/Inventati is a collective that recognizes and promotes
**anticapitalism**, **antiracism**, **antifascism**, **antisexism**,
**antimililtarism**, and the **refusal of authoritarianism and hierarchies**. We
all work at this project on a volunteer basis. If we do it, it's because we
firmly believe in these principles, and the people we want to offer these
services to are the people who share these principles.
Therefore, when you explain why you got to this service request form, don't tell
us that you want privacy and/or anonymity - we already know this, otherwise you
wouldn't have landed here.
Tell us, instead, the reasons why you share these principles and what kind of
person you are. You don't have to detail your private life - we just need to get
an idea of why we should offer a service to you.

If you would like to know something more about us, read our [manifesto](/who/manifesto),
our [policy](/who/policy) and the [privacy policy](/who/privacy-policy).

Every request will be read by a real person, not by a robot. So we will
appreciate every information you decide to share with us, and hope we can
establish a trust relationship, starting from the fact that your request will be
eventually destroyed.

But trust is mutual: just as we will believe in what you say without asking for
your personal data, if we see that you're violating our principles publicly
while using our services, we won't hesitate to delete your account without
previous notice. If you want to use our services, make sure this is the right
place for you.

We also receive many requests from people who want to "be free from control by
multinational corporations and the NSA". Our services have features that **make
it possible** to attain a level of privacy that can satisfy this need, but as
always, no political problem has a merely technical solution.
First of all, bear in mind that **the way you use our services is what really
counts for your privacy** - in other words, don't expect that using our services
is like pushing a magic button that makes you untraceable. If, for example, you
use your A/I account to subscribe to services that track your IP address, are
connected to your phone number or systematically map and record your social
interactions, having an anonymous email account won't help much.

**Summing up, you are the one who is responsible for your privacy** - we just
make this process technically possible, but only when and if the way you use
these tools allows this.

We hope that you use the technology and communication tools we are offering you
mindfully, and that besides this you are also ready to claim, *in real life*,
your freedom of expression and communication.

Finally, please remember that **we deliver services for free to our end-users, but we have some infrastructure costs**. 
Keeping these servers up and running requires more than simple routine maintenance (which happens on a volunteer basis and without any remuneration),
so please consider making at least a small donation to help us cover the expenses. 

[Find out how to donate to Autistici/Inventati](/donate "donate").

**[Request a service](https://services.autistici.org/)**
