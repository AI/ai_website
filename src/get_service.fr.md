title: Demande un service
----

# Demande un service

Certains services offerts par A/I, tel que [IRC](/services/chat#irc "IRC - Chat") et les outils d'anonymat, sont
destinés à une utilisation libre par quiconque surfant sur Internet, tandis que d'autres services "personnels" nécessitent un enregistrement
et la création d'un *compte* sur A/I.
 Lorsque vous vous enregistrez, nous ne vous demandons aucune information personnelle, à part une adresse e-mail sur laquelle nous pourrons
vous envoyer vos donn&eacut;es d'activation (puisque toutes les requêtes doivent être approuvées) - mais ça peut aussi vous aider à vous
rappeler à quel point il est difficil d'effacer toutes vos traces... (Nous ne gardons aucune trace de quel e-mail nous a demandé quel
compte, mais votre fournisseur d'e-mail peut-être...)

Tout ce que nous demandons à nos utilisateurs c'est de partager nos principes d'antifascisme, antisexisme, antiracisme, antimilitarisme et
d'utilisation non-commerciale. Veuillez lire notre [politique](/who/policy) et notre
[manifeste](/who/manifesto) avant de demander un compte.

[Privacy Policy](/who/privacy-policy)

Nous tenons à préciser qu'être hébergé sur notre réseau ne signifie pas que nos services sont gratuits : cela signifie que nous avons choisi
une approche autogérée et que nous voulons rester à l'écart des intérêts commerciaux et des jeux de pouvoir. Ce qui est plus le important
c'est que cela ne signifie pas seulement que vous pouvez utiliser nos services, mais que vous devez veiller à la survie de ce lieu. Etant
donnés que garder ces serveurs implique plus que simplement les maintenir et les surveiller tous les jours (ce que nous faisons bénévolement
sans aucune contrepartie économique), nous vous serions reconnaissants si vous pouviez faire un don, même petit, pour nous aider à subvenir
aux dépenses importantes liées à l'hébergement, la connection, la maintenance matérielle et ainsi de suite. [Voir ici comment faire un don à
A/I](/donate "donate")

Si vous souhaitez demander un compte et/ou un service, suivez ces quelques étapes:

## 1 - Lisez attentivement [notre politique](/who/policy)

Demander un de nos services implique automatiquement que vous acceptez les principes exprimés dans notre politique.

## 2 - Suivez ce lien: [https://services.autistici.org/](https://services.autistici.org/)

et remplissez le tableau de demande d'activation d'un nouveau service: faites attention de bien remplir correctement toutes les cases et n'oubliez pas de nous laisser une adresse e-mail pour vous contacter. Nous serions également heureux de savoir ce que vous comptez faire du service que vous demandez, ce genre de choses.

Nous nous interessons à nos utilisateurs et nous aimons à penser qu'ils s'interessent à nous en échangeant des idées, des plans et des projets avec A/I.

## 3 - Attendez l'e-mail de confirmation

(ou le refus si nous considérons que votre demande ne convient pas à notre politique): nous vous enverrons toutes les informations et les instructions nécessaires pour accéder à votre compte et à votre [panneau d'utilisateur](/docs/userpanel) (où vous pourrez trouver tous les services connectés à votre compte!) Le collectif A/I fait de son mieux pour tenir le rythme de toutes les demandes, alors croyez nous : tôt ou tard vous recevrez une réponse.
