title: Aidez nous à traduire le site dans votre langue!
----

Aidez nous à traduire le site dans votre langue!
================================================

Si vous êtes ici c'est que vous avez vu une page en anglais plutôt que dans votre langue préférée.

Vous pouvez nous aider à traduire le site et ainsi contribuer au projet!

Pour ce faire, envoyez nous un mail avec la traduction en pièce jointe à `trans _at_ autistici.org` en copiant l'adresse URL de la page dans le Sujet.
