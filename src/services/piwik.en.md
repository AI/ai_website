title: Web statistics
----

Web statistics
==============

After years and years of requests, pleas and threats, finally our keen users can have a service which counts visits in their sites and
blogs.

A/I Collective's new web statistics include both websites hosted in our servers and [Noblogs.org](http://noblogs.org) blogs, and are based
on [Matomo](http://matomo.org), which is free software.

Activating this kind of tool within our services wasn't an easy decision for A/I, since we prefer not to keep users' data, but in this case
we weighed up the pros and cons, and the pros won.

The point is that gathering web statistics means monitoring every visit to the relevant websites and identifying their origin, and this
activity can be easily associated with user profiling, which is often linked with market research and log keeping. But contrary to
commercial services as GoogleAds, in our case we only survey some details, such as the URLs from which visits originate: instead, we don't
keep IPs, the addresses associating visits to real persons, and we also got rid of the most intrusive plugins like the ones checking the
country of origin of each visit and the visitor's screen size.

Besides, by using plugins against profiling such as Adblock on Firefox, visitors can hide from Piwik's monitoring. For the owner of the site
this can be a drawback because she won't be able to count every visit, but for web surfers it has the advantage of offering the possibility
not to be counted. However, you can suggest your friends to disable Adblock just for your website/blog and the problem will be partly
solved.

If it's you who don't like the idea of profiling your readers, don't worry: Piwik is not activated by default, but only if a website/blog
admin chooses to do so.

Our new webstat tool is automatically active on [Noblogs.org blogs](http://noblogs.org) but **you have to activate it on hosted sites, using this [howto](/docs/web/webstats)**

Consider however that this service is public and therefore anyone can look up your statistics. This also implies that you can publish your
stats in your website, by using a number of widgets you can access from the stats dashboard.

In any case, we warmly suggest you to use Piwik if you are currently using a commercial counter (like Shinystat or Google analitics):
actually, as you will have already read in our [policy](/who/policy), this kind of commercial services must be
absolutely avoided in A/I’s web spaces since it conflicts with anonymity protection we try to implement as best as possible.
