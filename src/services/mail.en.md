title: Email
----

Email
======

[Having a mailbox](/get_service) on our servers is a first step to safeguard own privacy, avoiding the need to give away personal data or relationships to
multinationals or to some commercial provider. Furthermore we won't ask you your name or other personal information to activate your mailbox.

A/I mailboxes have no space limit but we hope you will use your common sense to not waste precious resources you share with many other users
(and remember, [donate](/donate "donate"))! We strongly recommend you to **download** your mail whenever possible, also to minimize the amount 
of personal private data present at any time on the servers.

A/I mailbox service offers you a wide range of domains to choose from.... [pick your flavour!](/get_service)

And with your mailbox account you can create up to **five** more addresses (as aliases of your same mailbox account)!

**Remember that if you do not read your mailbox for more than 12 months, the mailbox is disabled and its content deleted.**


If you have any questions on how to use our mailbox, please refer to the following manuals:

- [Connection parameters](/docs/mail/connectionparms "connection parameters for A/I email users")
- [Technical notes on A/I e-mail service](/docs/mail/tech_mail "technical notes on A/I e-mail service")
- [Two-factor authentication](/docs/2FA "Two-factor authentication")
- [Very simple basic rules to protect your privacy while using emails](/docs/mail/privacymail "very simple basic rules to protect your privacy while using emails")
- [How to change or recover your account password](/docs/mail/passwd "how to change or recover your password")
- [How to create a safe password](/docs/mail/passwd_safe "how to create a safe password")
- [Roundcube A/I webmail howto](/docs/mail/roundcube "howto use A/I webmail")
- [Howto setup webmail filters](/docs/mail/sieve "howto setup filters in the A/I webmail")
- [Howto setup Evolution mail client for A/I](/docs/mail/evolution "howto setup Evolution mail client for A/I")
- [Howto setup Fetchmail mail client for A/I](/docs/mail/fetchmail "howto setup Fetchmail mail client for A/I")
- [Howto setup mutt+msmtp as mail client for A/I](/docs/mail/mutt-msmtp "howto setup muttmsmtp mail client for A/I")
- [Howto setup Thunderbird mail client for A/I](/docs/mail/thunderbird "howto setup Thunderbird mail client for A/I")
- [Howto setup Sylpheed mail client for A/I](/docs/mail/sylpheed "howto setup Sylpheed mail client for A/I")
- [Howto setup Postfix to handle A/I mail](/docs/mail/postfix "howto setup Postfix to handle A/I mail")
- [Howto setup Mail OSX mail client for A/I](/docs/mail/mail-osx "howto setup Mail OSX mail client for A/I")
- [Howto setup Outlook mail client for A/I](/docs/mail/outlook "howto setup Outlook mail client for A/I")
- [How to setup an A/I mail account on your iPhone](/docs/mail/iphone "howto setup your AI/ mail account on your iPhone")
- [How to setup an A/I mail account on Android with K-9 Mail](/docs/mail/k9mail "howto setup your AI/ mail account on Android K-9 mail")

Why should I make a mailbox account on A/I?
-------------------------------------------


- because i think the struggle to defend free speech and communication cannot be delegated to anybody.
- because i want to make a small difference in my daily life, to keep away from multinational corporations.
- because for some reasons, (the wrong ones, you should never trust anyone, mind you!) i trust you.
- because i prefer my mailbox to be a private matter.
- because i don't want to help advertising a big company when i give someone my mail address.
- because i am annoyed by those who give me an email account for free and then send me tons of banners and promotional offers.
- because when someone tries to shut down a/i services or threaten their security, the a/i collective will do their best to
    avoid it. And the more we are, the more it will be interesting.
- because i like your domains.
- because the more we are, the louder we are.


A/I vs Gmail: upsides and downsides of choosing us
--------------------------------------------------

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<tbody>
<tr class="odd">
<td align="left"></td>
<td align="left">A-I</td>
<td align="left">G-mail</td>
</tr>
<tr class="even">
<td align="left">Available space</td>
<td align="left">no limits, but please, DOWNLOAD your mail!</td>
<td align="left">no limits: they keep a record of everything (at their will).</td>
</tr>
<tr class="odd">
<td align="left">POP3 and POP3-SSL</td>
<td align="left">YES (letsencrypt certificates)</td>
<td align="left">YES (Google INC certificates)</td>
</tr>
<tr class="even">
<td align="left">IMAP and IMAP-SSL</td>
<td align="left">YES (letsencrypt certificates)</td>
<td align="left">YES (Google INC certificates)</td>
</tr>
<tr class="odd">
<td align="left">SMTP TLS</td>
<td align="left">YES (letsencrypt certificates)</td>
<td align="left">YES (Google INC certificates)</td>
</tr>
<tr class="even">
<td align="left">Content analysis of your mail</td>
<td align="left">NO, never.</td>
<td align="left">YES, for customized advertising.</td>
</tr>
<tr class="odd">
<td align="left">Logging (tracing) of the IPs you connect from</td>
<td align="left">No, never.</td>
<td align="left">Yes, always.</td>
</tr>
<tr class="even">
<td align="left">SSL Webmail</td>
<td align="left">YES (letsencrypt certificates)</td>
<td align="left">YES (Google INC certificates)</td>
</tr>
<tr class="odd">
<td align="left">banners in webmail</td>
<td align="left">NO</td>
<td align="left">YES (text format)</td>
</tr>
</tbody>
</table>

What does it actually mean to "have an e-mail account"?
-------------------------------------------------------

For those who really don't know what e-mail means, we would like to help you.

An email account is exactly like having a real mailman in your computer: you write a letter, you send it to an addressee and the mail is
delivered through a series of servers (a server is a computer managing a service) between the place where you are and your mail destination.

Having an e-mail account means having a name and an address where people can send you mails.

There are two main ways of reading e-mail messages: you can browse them on the Internet (Firefox, Netscape, Konqueror, Galeon, Explorer,
Opera and so on and so forth) or you can download them and send them through a software on your computer (a software like Thunderbird,
Netscape Messenger, Kmail, Pegasus Mail, Outlook Express, Eudora or any other "mail client" able to "discuss" with your mail server.

Of course, apart from reading e-mail messages, you can also send them, both using a browser and using a mail client as in the previous
paragraph. In order to send your mail, you use a service called SMTP, designed for taking your letters and delivering them to your
addressee, wherever she may be.

You should know that normally all these communications are not encrypted along the way, and they can be easily read at any of the nodes of
the Internet they travel through.

It's not so unthinkable that if someone wanted to control you, reading your messages as though they were flyers left on the sidewalk
wouldn't take her so much time and effort... Therefore, it would be more correct to compare an e-mail message with a postcard rather than
with a letter closed in an envelope.

To limit these abuses (and not to exclude any form of control, but to limit it), you can choose to send and download your e-mail via SSL,
i.e. encrypted (coded so as not to be easily readable by anyone).
