title: SSL: un tunnel crittato per mettere al sicuro le nostre comunicazioni 
----

# SSL: un tunnel crittato per mettere al sicuro le nostre comunicazioni

Quando vi connettete a un sito tramite **https** e quando configurate il vostro
client di posta o il programma che usate per chattare in modo da usare la
cifratura SSL, il vostro computer riceve dai server di A/I un certificato SSL,
che permette di creare un tunnel crittato per proteggere le vostre
comunicazioni.

Per molto tempo A/I ha scelto di firmare i certificati SSL senza ricorrere a
un'autorità di certificazione commerciale, perché questo avrebbe significato
mettere il rapporto di fiducia tra noi e i nostri utenti nelle mani di soggetti
di cui non ci si può fidare: stati, forze “dell’ordine”, aziende il cui solo
fine è il profitto.

Poi a dicembre 2015 è stata lanciata [Let's Encrypt](https://letsencrypt.org/),
un'autorità di certificazione (fondata, tra l'altro, dall'[Electronic Frontier
Foundation](https://www.eff.org/)) che fornisce certificati SSL gratuiti e
facili da installare con lo scopo dichiarato di agevolare e diffondere questo
tipo di cifratura.
Per questo ora tutti i servizi di A/I sono crittati con chiavi SSL firmate da
Let's Encrypt, che i vostri browser e i vostri client riconoscono
automaticamente (se aprite questa pagina in HTTPS invece che in HTTP, noterete
un lucchetto verde nella barra degli indirizzi, che indica che la connessione è
sicura).

Questo significa che non dovete più fare niente per stabilire connessioni
sicure con i nostri server, e semmai conviene controllare che i vostri client
siano tutti configurati per non accettare certificati SSL non validi (come per
esempio consigliavamo invece un tempo di fare per Xchat).

L'unico servizio per cui non ci basiamo su Let'Encrypt ma compriamo un
certificato commerciale è [Noblogs](https://noblogs.org/): la struttura della
piattaforma ci costringerebbe infatti a creare un certificato per ciascun blog,
e questo sarebbe uno spreco di risorse per Let's Encrypt. La soluzione è creare
un certificato wildcard (che serve a mettere al sicuro più domini allo stesso
tempo), cosa che purtroppo è (ancora) impossibile con Let's Encrypt. 
