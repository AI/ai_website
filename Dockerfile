FROM debian:stable AS build
ADD . /src
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends rsync git golang ca-certificates && cd /src && ./scripts/lint.sh && ./scripts/update.sh

FROM registry.git.autistici.org/ai3/docker/apache2-base:master

COPY --from=build /src/public /var/www/autistici.org
COPY --from=build /src/build/bin/sitesearch /usr/sbin/sitesearch
COPY --from=build /src/index /var/lib/sitesearch/index
COPY templates /var/lib/sitesearch/templates
COPY docker/conf /tmp/conf
COPY docker/build.sh /tmp/build.sh

RUN /tmp/build.sh && rm /tmp/build.sh

ENTRYPOINT ["/usr/local/bin/chaperone"]
