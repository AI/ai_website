    /* @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt MIT */

ai = {
    'lang': 'en'
};

// Each page should have a public URL ('url') and a language attribute
// ('lang'). For local data read from all_pages.json, we extract these
// values from the full internal page URL. This function also turns the
// site map (a dictionary) into an array.
ai.fix_local_pages_meta = function(sitemap) {
    var out = [];
    var langRx = /(index)?\.([a-z]+)\.html$/;
    $.each(sitemap, function(unusedPath, pagemeta) {
        if (pagemeta.title != "") {
            var langMatch = langRx.exec(pagemeta.url);
            if (langMatch.length > 2) {
                pagemeta.lang = langMatch[2];
            }
            pagemeta.url = pagemeta.url.replace(langRx, '');
            out.push(pagemeta);
        }
    });
    return out;
};

// Returns the first attribute of obj in attrlist that is set.
ai.pick_attr = function(obj, attrlist) {
    for (i = 0; i < attrlist.length; i++) {
        if (obj.hasOwnProperty(attrlist[i])) {
            return obj[attrlist[i]]
        }
    }
    return null;
}

// How to build a language-specific autocompletion list for typeahead.js:
//
// We don't want to show 10 versions of the same page in different
// languages, but translation coverage is uneven (except for English,
// which we will use as a fallback). So, we unscramble the page list
// by first aggregating pages by their public URL, then we pick the
// right page for the current language (or English as a fallback).
//
ai.transform_local_pages = function(sitemap) {
    pagelist = ai.fix_local_pages_meta(sitemap);
    var byLang = {};
    $.each(pagelist, function(idx, p) {
	var url = p.url;
	if (!byLang.hasOwnProperty(url)) {
	    byLang[url] = {};
	}
	byLang[url][p.lang] = p;
    });

    //console.log('byLang: ' + JSON.stringify(byLang));

    var out = [];
    $.each(byLang, function(url, langmap) {
        var tmp = ai.pick_attr(langmap, [ai.lang, 'en', 'it']);
        if (tmp) {
            out.push(tmp);
        }
    });

    //console.log('out: ' + JSON.stringify(out));

    return out;
};

// Transform results from a remote /search to a list suitable for
// autocompletion, according to the same rules mentioned in
// transform_local_pages.
ai.transform_remote_pages = function(response) {
    var out = [];
    // Maybe undefined, maybe null?
    if (typeof(response.result.hits) == 'undefined' || !response.result.hits) {
        return out;
    }
    $.each(response.result.hits, function(idx, h) {
        out.push(h.fields);
    });
    console.log('remote results: ' + JSON.stringify(out));
    return out;
};

$(function() {
    // Autofocus the search form when the search dropdown is clicked.
    $('.dropdown-toggle').click(function() {
    //$('#searchBtn').click(function() {
    
    	setTimeout(function(){$('input[name=q]').focus()}, 150);
    	
	// $('#searchForm input:first').focus();
        return true;
    });

    // Allow user to set the language.
    $(".set-lang").click(function() {
        var lang = $(this).attr("data-language");
        document.cookie = "site_language=" + lang + "; path=/";
        // console.log('language set to ' + lang);
        location.reload(true);
    });

    // Initialize typeahead.
    var search = new Bloodhound({
	datumTokenizer: function(datum) {
            return Bloodhound.tokenizers.whitespace(datum.title);
        },
	identify: function(datum) {
	    return datum.url;
	},
        sorter: function(a, b) {
            if (a.title < b.title) {
                return -1;
            }
            if (a.title > b.title) {
                return 1;
            }
            return 0;
        },
	queryTokenizer: Bloodhound.tokenizers.whitespace,
	prefetch: {
	    url: '/all_pages.json',
	    transform: ai.transform_local_pages,
	    thumbprint: 'autocomplete_' + ai.lang
	},
	remote: {
	    url: '/search?fmt=json&q=%QUERY',
	    wildcard: '%QUERY',
            transform: ai.transform_remote_pages,
            thumbprint: 'remote_autocomplete_' + ai.lang
	},
	sufficient: 3
    });
    $('.typeahead').typeahead({
	hint: true,
	highlight: true,
	minLength: 1
    }, {
	name: 'search',
	source: search,
	limit: 20,
	display: function(suggestion) {
	    return suggestion.title;
	}
    });
    $('.typeahead').bind('typeahead:select', function(ev, suggestion) {
	console.log('Selected: ' + suggestion);
        if (suggestion.hasOwnProperty('url')) {
	  location.href = suggestion.url;
	}
    });
});
    /* @license-end */