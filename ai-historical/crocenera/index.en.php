<html>
<head>
  <title>Crocenera's mailbox precautionary seize</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Crocenera's mailbox precautionary seize filiarmonici"/>
  <meta name="keywords" content="Crocenera's mailbox precautionary seize filiarmonici"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <h3>Crocenera's mailbox precautionary seize</h3>
  <small>Documentation about crocenera@inventati.org mailbox precautionary seize</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian</a> | <a href="index.en.php">english</a> | <a href="index.es.php">spanish</a>]<br/>

  <hr/>

  <ul>
   <li>Press releases<br>
	<ul>
	<li>
   	<a href="http://italy.indymedia.org/news/2005/05/799042.php?xs=y">
	filiarmonici's press release</a>
	</li>
	</ul>
   </li>
  </ul>

  <hr/>
  <ul>
   <li>Mirror Filarmonici Web Site<br>
        <ul>
        <li>
        <a href="http://web.archive.org/web/20041020053714/http://www.filiarmonici.org/crocenera.html">
        web.archive.org</a>
        </li>
        <li>
        <a href="http://squat.net/filiarmonici/crocenera/">
        squat.net</a>
        </li>
        <li>
        <a href="http://riseup.net/filiarmonici/crocenera.html">
        riseup.net</a>
        </li>


        </ul>
   </li>
  </ul>



  <ul>
   <li>Discussion about eavesdropping in Italy (italian only)<br>
	<ul>
	<li>
	<a href="http://punto-informatico.it/p.asp?i=52273">
	Sotto l'occhio del grande orecchio<br>
	</a>
	</li>
	<li>
	<a href="http://punto-informatico.it/p.asp?i=51648">
	Intercettazioni, gli operatori si scaldano<br>
	</a>
	</li>
	</ul>
  </ul>
  <hr/>

  <a href="http://www.inventati.org">Home Page del progetto Autistici/Inventati</a><br/>

 </div>


 <div id="main">




<h2>
Crocenera's mailbox precautionary seize
</h2>

<br>
<br>
<h2><b>Press release on 27/05/2005</b></h2>
<p>
May 26, in the morning, we received one fax and several calls from digos (italian political police) about removing a mail account on our server. The mail box is
</p>
<p>
   croceneraanarchica(at)inventati.org
</p>
<p>
In the same time was announced the seize of 
</p>
<p>
   <a href="http://www.filiarmonici.org/crocenera.html">
   http://www.filiarmonici.org/crocenera.html
   </a>
</p>
<p>
The request is part of a national police operation which leaded to several arrests and searches. For more info read:
</p>
<p>
   <a href="http://italy.indymedia.org">
   http://italy.indymedia.org
   </a>
</p>
<p>
   and also:
</p>
<p>
   <a href="http://www.anarcotico.net">
   http://www.anarcotico.net
   </a>
</p>
<p>
We've been forced to remove the mail account, as written in the seize decree which you can read here:
</p>
<p>
   <a href="http://italy.indymedia.org/news/2005/05/798576.php">
   http://italy.indymedia.org/news/2005/05/798576.php
   </a>
</p>
<p>
   This is the first time we (as an indipendent server) get such a request. We were not requested, as in the past, to surrender utilization data for a certain mailbox, but to remove the account itself. Anyway we wouldn't have been able to give back such infomations, because of our no-log approach on the servers we manage: we don't save any personal data about autistici/inventati's resource utilization and we can't link a mail o web account to a physical person. Examining the decree it's clear that communication tracing by police inqueries is getting better by practice. email monitoring by police is not quantifiable and often involves big business players, as in this case hotmail.com. A common commercial provider has no need to guarantee its users' privacy. This fact was clear since Cosenza inquiry. Look:
</p>
<p>
   <a href="http://www.autistici.org/it/stuff/archive/newsletter/20021119-comunicato-arresti-cosenza.html">
   http://www.autistici.org/it/stuff/archive/newsletter/20021119-comunicato-arresti-cosenza.html
  </a>
<p>
(in italian) in which part of the prosecution material came from commercial email boxes monitored by the police. The need/right for privacy and anonymity are part of our project's basical points, and we think they are the inner part of any form of communication.
</p>
<p>
   Looking over the single case we think is worringly clear how easy are eavesdropping, sizing and evictions by police and judges. Some months ago, on february 25th, in some newspapers and specifically in "il sole 24 ore", were shown articles about Telecom eavesdropping problem: Telecom italy claimed to not have enough technical resources for satisfying all the judges requests about eavesdropping. It's interesting, bordering on the ridiculous, but it's a good example to understand the problem size. It's an abuse proved by facts and not just our paranoia. The eavesdropping market is expanding: 300 millions of euros for year 2004, 140 thousands cellphones eavesdrops and 120 thousands telephone print-outs returned to the judges . These numbers were provided by a special issue of "Repubblica", now part of the mass media posse involved in the lynching of the people arrested and searched last week. You can read the full article on line here (italian language):
</p>
<p>
   <a href="http://www.repubblica.it/2004/k/sezioni/cronaca/intercett/intercett/intercett.html">
   http://www.repubblica.it/2004/k/sezioni/cronaca/intercett/intercett/intercett.html
  </a>
</p>
<p>
The idea of a free and uncensorable network is waning, as happens to freedom in real life. In a world where the authorities constantly and consciously read our lifes like an open book, we feel less free, but even more willing to defend and enlarge our freedoms by any means necessary. These few words are the first we feel able to publish. For a deeper analysis of the facts, we point you to the next joint press release with ecn.org.
</p>


  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Page changed on the
    29/09/2010   </em></small>
  </p>


 </div>

</body>
</html>
