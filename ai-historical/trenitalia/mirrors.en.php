<html>
<head>
  <title>Mirrors</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Citazione della compagnia ferroviaria italiana contro l'associazione Autistici Inventati. Raccolta atti giudiziari del processo per la chiusura di un sito di satira. Case Study."/>
  <meta name="keywords" content="Clienti Insoddisfatti, Sindacato ferrotranvieri, Censura contro la satira, diritto alla satira. sito censurato, guerra in iraq, trasporto armi, trasporto materiale bellico, liberta' di espressione, boicottare, pendolari, orario dei treni"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <img align="right" clear="none" src="materiali/banner_ai_half.png" alt="banner autistici/inventati sotto attacco"/>
  <h3>A wagonful of...</h3>
  <small>Trenitalia Vs Autistici/Inventati</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian version</a>] -  [<a href="index.en.php">english version</a>]<br/>

  <hr/>

  <ul>
   <li><a href="mirrors.en.php">Mirrors</a></li>
   <li><a href="legaldocs.en.php">Legal Documentation [only ITA]</a></li>
   <li><a href="rassegna.en.php">Web/Press Release</a></li>
   <li><a href="comunicato_zenmai23.php">Zenmai23 statement [only ITA]</a></li>
   <li><a href="link.php">Links about Trenitalia</a></li>
  </ul>

  <hr/>

  <ul>
   <li><a href="protesta.en.php">What can I do?</a></li>
	<ol>
	<li><a href="protesta.en.php#mail">Send an e-mail to Trenitalia</a></li>
	<li><a href="protesta.en.php#banner">Put our banner on your site</a></li>
	<li>Sign the <a href="http://www.autistici.org/ai/trenitalia/guest/guestbook.php">Guestbook</a></li>
	<li><a href="protesta.en.php#signature">Use this signature!</a></li>
	<li><a href="protesta.en.php#stampa">Printed material for divulgation</a></li>
	<li>Make a <a href="http://autistici.org/it/donate.html">donation!</a></li>
	</ol>
  </ul>

  <hr/>

  <a href="http://www.inventati.org">Home Page Autistici/Inventati</a><br/>

 </div>


 <div id="main">


<h2>Mirrors</h2>

<p>
Incredible. The Internet is really a tricky place... We just discovered, to our own surprise, that the "awfully offensive" content has reproduced
itself in the most disparate places, totally beyond our control. We apologize, but we have no idea on how to stop this phenomenon! 
</p>



<ul>

<li><a href="http://riseup.net/zenmai23/trenitalia/">http://riseup.net/zenmai23/trenitalia/</a></li>
<li><a href="http://threespeed.org/zenmai23/trenitalia/index.htm">http://threespeed.org/zenmai23/trenitalia/</a></li>
<li><a href="http://www.sindominio.net/~seajob/zenmai23/trenitalia/">http://www.sindominio.net/~seajob/zenmai23/trenitalia/</a></li>
<li><a href="http://italy.indymedia.org/zenmai23/trenitalia/">http://italy.indymedia.org/zenmai23/trenitalia/</a></li>
<!--<li><a href="http://zenmai.kicks-ass.org/">http://zenmai.kicks-ass.org/</a></li>-->
<li><a href="http://zenmai23.linefeed.org/trenitalia/">http://zenmai23.linefeed.org/trenitalia/</a></li>
<li><a href="http://scii.nl/~zenmai23/trenitalia/">http://scii.nl/~zenmai23/trenitalia/</a></li>
<li><a href="http://tazebao.dyne.org/zenmai23/trenitalia/">http://tazebao.dyne.org/zenmai23/trenitalia/</a></li>
<li><a href="http://zenmai23.altervista.org/trenitalia/">http://zenmai23.altervista.org/trenitalia/</a></li>
<li><a href="http://tuxic.nl/mirrors/zenmai23/trenitalia/">http://tuxic.nl/mirrors/zenmai23/trenitalia/</a></li>
<li><a href="http://www.transhack.org/zenmai23/trenitalia/">http://www.transhack.org/zenmai23/trenitalia/</a></li>
<li><a href="http://znetit.mahost.org/trenitalia/">http://znetit.mahost.org/trenitalia/</a></li>
<li><a href="http://membres.lycos.fr/zenmai23/trenitalia/">http://membres.lycos.fr/zenmai23/trenitalia/</a></li>
<li><a href="http://zenmai23.latinowebs.com/">http://zenmai23.latinowebs.com/</a></li>
<li><a href="http://sindominio.net/trenitalia/">http://sindominio.net/trenitalia/</a></li>

</ul>


  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Pagina modificata il 
    13/01/2007   </em></small>
  </p>


 </div>

</body>
</html>
