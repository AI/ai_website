_menuCloseDelay=0        // The time delay for menus to remain visible on mouse out
_menuOpenDelay=0           // The time delay before menus open on mouse over
_subOffsetTop=-1             // Sub menu top offset
_subOffsetLeft=-5         // Sub menu left offset

with(menuStyle=new mm_style()){
onbgcolor="#FADE9C";
oncolor="#333333";
offbgcolor="#FFFFFF";
offcolor="#333333";
bordercolor="#333333";
borderstyle="solid";
borderwidth=1;
separatorcolor="#333333";
separatorsize="1";
separatorheight="10";
padding=1;
fontsize="11";
fontweight="bold";
fontfamily="Verdana, Tahoma, Arial";
pagecolor="black";
pagebgcolor="#FFFFFF";
headercolor="#000000";
headerbgcolor="#ffffff";
subimage="http://bellaciao.org/images/puce2.gif";
subimagepadding="1";

}

with(menuStyle2=new mm_style()){
onbgcolor="#F5F5F5";
oncolor="#000000";
offbgcolor="#FADE9C";
offcolor="#333333";
bordercolor="#333333";
borderstyle="solid";
borderwidth=1;
padding=1;
fontweight="bold";
subimage="http://bellaciao.org/images/puce3.gif";
subimagepadding="1";
}

with(bellaciao=new menuname("Main Menu")){
style=menuStyle;
top=103;
left=130;
margin=0;
alwaysvisible=1;
orientation="horizontal";
aI("text=&nbsp;Accessibilit�;showmenu=Accessibilit�;url=http://bellaciao.org/fr/oo;title=Terminal en mode texte (VT, tablette braille, synth�tiseur vocal, bas d�bit...);status=Terminal en mode texte (VT, tablette braille, synth�tiseur vocal, bas d�bit...);");
aI("text=&nbsp;Accueil;showmenu=Accueil;url=http://bellaciao.org/fr/;title=Accueil;status=Accueil;");
aI("text=&nbsp;Le collectif;showmenu=Le collectif;url=http://bellaciao.org/fr/collectif.php;title=Le collectif Bellaciao;status=Le collectif Bellaciao");
aI("text=&nbsp;Dossiers;showmenu=Dossiers;status=Dossiers;");
aI("text=&nbsp;Culture;showmenu=Culture;url=http://bellaciao.org/fr/culture.php;status=Culture;title=Culture;");
aI("text=&nbsp;M�dias;showmenu=M�dias;status=M�dias;title=M�dias;");
aI("text=&nbsp;Liens&nbsp;;url=http://bellaciao.org/fr/liens.php;status=Liens;title=Liens;");
aI("text=&nbsp;Communiquer;showmenu=Communiquer;status=Communiquer;title=Communiquer;");
}

with(bellaciao=new menuname("Accessibilit�")){
style=menuStyle2;
margin=3;
overflow="scroll";
aI("text=Plan du site TXT;url=http://bellaciao.org/fr/plan_txt.php3;status=Plan du site en MODE TEXTE : terminal en mode texte (VT, tablette braille, synth�tiseur vocal, bas d�bit...);title=Plan du site en MODE TEXTE : Terminal en mode texte (VT, tablette braille, synth�tiseur vocal, bas d�bit...);");
aI("text=Bellaciao TXT;url=http://bellaciao.org/fr/bellaciao_txt.php3;status=Les articles Bellaciao en MODE TEXTE;title=Les articles Bellaciao en MODE TEXTE;");
aI("text=Articles TXT;url=http://bellaciao.org/fr/articles_txt.php3;status=Tous les articles en MODE TEXTE;title=Tous les articles en MODE TEXTE;");
aI("text=Mots-cl� TXT;url=http://bellaciao.org/fr/mots_txt.php3;status=Les mots-cl� en MODE TEXTE;title=Les mots-cl� en MODE TEXTE;");
}

with(bellaciao=new menuname("Accueil")){
style=menuStyle2;
margin=3;
overflow="scroll";
aI("text=Accueil FR;url=http://bellaciao.org/fr/;");
aI("text=Accueil IT;url=http://bellaciao.org/it/;");
aI("text=Accueil EN;url=http://bellaciao.org/en/;");
aI("text=Agenda;url=http://bellaciao.org/fr/rubrique.php3?id_rubrique=2;status=Agenda;title=Agenda;");
aI("text=Recherche;url=http://bellaciao.org/fr/recherche.php;status=Recherche;title=Recherche;");
}

with(bellaciao=new menuname("Le collectif")){
style=menuStyle2;
margin=3;
overflow="scroll";
aI("text=Qui sommes-nous?;url=http://bellaciao.org/fr/collectif/qui_sommes_nous.php;status=Qui sommes-nous?;title=Qui sommes-nous?;");
aI("text=Articles de Bellaciao;url=http://bellaciao.org/fr/collectif.php;status=Articles de Bellaciao;title=Articles de Bellaciao;");
aI("text=Ev�nementiel;url=http://lescarabee.com/archives;status=Ev�nementiel;title=Ev�nementiel;");
aI("text=Souscription;url=http://bellaciao.org/souscription.php;status=Souscription;title=Souscription;");
}

with(bellaciao=new menuname("Dossiers")){
style=menuStyle2;
margin=3;
overflow="scroll";
aI("text=Amen.fr;url=http://bellaciao.org/amen;status=Amen.fr;title=Amen.fr;");
aI("text=AGCS;url=http://bellaciao.org/fr/agcs.php;status=AGCS;title=AGCS;");
aI("text=Elections;url=http://bellaciao.org/fr/mot.php3?id_mot=49;status=Elections;title=Elections;");
aI("text=Environnement;url=http://bellaciao.org/fr/environnement.php;status=Environnement;title=Environnement;");
aI("text=Extraditions;url=http://bellaciao.org/fr/extraditions.php;status=Extraditions;title=Extraditions;");
aI("text=Forum Social;showmenu=Forum Social;url=http://bellaciao.org/fr/forum_social.php;status=Forum Social;title=Forum Social;");
aI("text=G8;showmenu=G8;url=http://bellaciao.org/fr/G8.php;status=G8;title=G8;");
aI("text=Guerres-conflits;url=http://bellaciao.org/fr/mot.php3?id_mot=30;status=Guerres & conflits;title=Guerres & conflits;");
aI("text=Le mouvement;url=http://bellaciao.org/fr/mouvement.php;status=Le mouvement;title=Le mouvement;");
aI("text=Religions;url=http://bellaciao.org/fr/religions.php;status=Religions;title=Religions;");
aI("text=Social;url=http://bellaciao.org/fr/social.php;status=Social;title=Social;");
aI("text=Visuels;url=http://bellaciao.org/tracts-affiches;status=Visuels;title=Visuels;");
}

with(bellaciao=new menuname("Culture")){
style=menuStyle2;
margin=3;
overflow="scroll";
aI("text=Autre Cin�ma;url=http://bellaciao.org/fr/autre-cinema.php;status=Coordination Pour un autre cin�ma;title=Coordination Pour un autre cin�ma;");
aI("text=Expos-Mus�es;url=http://bellaciao.org/fr/expos.php;status=Expos - Mus�es;title=Expos - Mus�es;");
aI("text=Cin�ma-Vid�o;url=http://bellaciao.org/fr/films.php;status=Cin�ma - Vid�o;title=Cin�ma - Vid�o;");
aI("text=Litt�rature;url=http://bellaciao.org/fr/litterature.php;status=Litt�rature - Po�sie;title=Litt�rature - Po�sie;");
aI("text=Musique;url=http://bellaciao.org/fr/musique.php;status=Musique - concerts - radio - disques...;title=Musique - concerts - radio - disques...;");
aI("text=Th��tre-danse;url=http://bellaciao.org/fr/theatre-danse.php;status=Th��tre - Danse;title=Th��tre - Danse;");
}

with(bellaciao=new menuname("M�dias")){
style=menuStyle2;
margin=3;
overflow="scroll";
aI("text=Cin�-Vid�o;url=http://bellaciao.org/fr/mot.php3?id_mot=26;status=Cin� - Vid�o;title=Cin� - Vid�o;");
aI("text=Internet;url=http://bellaciao.org/fr/internet.php;status=Internet;title=Internet;");
aI("text=Num�rique;url=http://bellaciao.org/fr/mot.php3?id_mot=59;status=Num�rique;title=Num�rique;");
aI("text=Photo;url=http://bellaciao.org/fr/mot.php3?id_mot=45;status=Photo;title=Photo;");
aI("text=Presse;url=http://bellaciao.org/fr/presse.php;status=Presse;title=Presse;");
aI("text=Radio;url=http://bellaciao.org/fr/mot.php3?id_mot=3;status=Radio;title=Radio;");
aI("text=T�l�vision;url=http://bellaciao.org/fr/mot.php3?id_mot=2;status=T�l�vision;title=T�l�vision;");
}

with(bellaciao=new menuname("Communiquer")){
style=menuStyle2;
margin=3;
overflow="scroll";
aI("text=Ecrire au collectif;url=mailto:bellaciaoparis@yahoo.fr;status=Ecrire au collectif;title=Ecrire au collectif;");
aI("text=Chatroom;url=http://bellaciao.org/irc/;status=Chatroom - canal IRC Bellaciao;title=Chatroom - canal IRC Bellaciao;");
aI("text=Newsletter;url=http://bellaciao.org/newsletter/;status=Lettre d'info du collectif Bellaciao;title=lettre d'info du collectif bellaciao;");
aI("text=Vous nous avez �crit;url=http://bellaciao.org/fr/courrier.php;status=Vous nous avez �crit;title=Vous nous avez �crit;");
}

with(bellaciao=new menuname("G8")){
style=menuStyle2;
margin=3;
overflow="scroll";
aI("text=G8 Evian 2003;url=http://bellaciao.org/fr/g8evian.php;status=G8 Evian 2003;title=G8 Evian 2003;");
aI("text=G�nes - 2001;url=http://bellaciao.org/fr/dossiers/genova/;status=G�nes 2001;title=G�nes 2001;");
}

with(bellaciao=new menuname("Forum Social")){
style=menuStyle2;
margin=3;
overflow="scroll";
aI("text=Forum Social;url=http://bellaciao.org/fr/forum_social.php;status=Forum Social;title=Forum Social;");
aI("text=Forum Social Europ�en 2004;url=http://bellaciao.org/fr/fse-2004.php;status=Forum Social Europ�en 2004;title=Forum Social Europ�en 2004;");
aI("text=Forum Social Europ�en 2003;url=http://bellaciao.org/fr/fse-2003.php;status=Forum Social Europ�en 2003;title=Forum Social Europ�en 2003;");
aI("text=Forum Social Europ�en 2002;url=http://bellaciao.org/fr/dossiers/FSE/FSE2002-Florence.php;status=Forum Social Europ�en 2002;title=Forum Social Europ�en 2002;");
}

drawMenus();
