<html>
<head>
  <title>Legal documentation</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Citazione della compagnia ferroviaria italiana contro l'associazione Autistici Inventati. Raccolta atti giudiziari del processo per la chiusura di un sito di satira. Case Study."/>
  <meta name="keywords" content="Clienti Insoddisfatti, Sindacato ferrotranvieri, Censura contro la satira, diritto alla satira. sito censurato, guerra in iraq, trasporto armi, trasporto materiale bellico, liberta' di espressione, boicottare, pendolari, orario dei treni"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <img align="right" clear="none" src="materiali/banner_ai_half.png" alt="banner autistici/inventati sotto attacco"/>
  <h3>A wagonful of...</h3>
  <small>Trenitalia Vs Autistici/Inventati</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian version</a>] -  [<a href="index.en.php">english version</a>]<br/>

  <hr/>

  <ul>
   <li><a href="mirrors.en.php">Mirrors</a></li>
   <li><a href="legaldocs.en.php">Legal Documentation [only ITA]</a></li>
   <li><a href="rassegna.en.php">Web/Press Release</a></li>
   <li><a href="comunicato_zenmai23.php">Zenmai23 statement [only ITA]</a></li>
   <li><a href="link.php">Links about Trenitalia</a></li>
  </ul>

  <hr/>

  <ul>
   <li><a href="protesta.en.php">What can I do?</a></li>
	<ol>
	<li><a href="protesta.en.php#mail">Send an e-mail to Trenitalia</a></li>
	<li><a href="protesta.en.php#banner">Put our banner on your site</a></li>
	<li>Sign the <a href="http://www.autistici.org/ai/trenitalia/guest/guestbook.php">Guestbook</a></li>
	<li><a href="protesta.en.php#signature">Use this signature!</a></li>
	<li><a href="protesta.en.php#stampa">Printed material for divulgation</a></li>
	<li>Make a <a href="http://autistici.org/it/donate.html">donation!</a></li>
	</ol>
  </ul>

  <hr/>

  <a href="http://www.inventati.org">Home Page Autistici/Inventati</a><br/>

 </div>


 <div id="main">


<h1>Legal documentation</h1>

<p>
The legal documentation regarding this trial is available to
the public. Unfortunately it is only in Italian...
<ol>
  <li><a href="documenti/1_denuncia_trenitalia/">citation from Trenitalia</a></li>
  <li><a href="documenti/2_memoria_autistici.txt">reply from Autistici/Inventati</a></li>
  <li><a href="documenti/3_replica_trenitalia/">reply from Trenitalia</a></li>
  <li><a href="documenti/4_controreplica_autistici.txt">counter-reply from Aut/Inv</a></li>
</ol>


  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Pagina modificata il 
    13/01/2007   </em></small>
  </p>


 </div>

</body>
</html>
