<html>
<head>
  <title>Mirrors</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Citazione della compagnia ferroviaria italiana contro l'associazione Autistici Inventati. Raccolta atti giudiziari del processo per la chiusura di un sito di satira. Case Study."/>
  <meta name="keywords" content="Clienti Insoddisfatti, Sindacato ferrotranvieri, Censura contro la satira, diritto alla satira. sito censurato, guerra in iraq, trasporto armi, trasporto materiale bellico, liberta' di espressione, boicottare, pendolari, orario dei treni"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <img align="right" clear="none" src="materiali/banner_ai_half.png" alt="banner autistici/inventati sotto attacco"/>
  <h3>E' arrivato un vagone carico di...</h3>
  <small>Documentazione sulla citazione da parte di Trenitalia contro Autistici/Inventati</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian version</a>] -  [<a href="index.en.php">english version</a>]<br/>

  <hr/>

  <ul>
   <li><a href="news.php">Aggiornamenti</a></li>
   <li><a href="mirrors.php">Mirrors</a></li>
   <li><a href="legaldocs.php">Documentazione legale</a></li>
   <li><a href="rassegna.php">Rassegna stampa/web</a></li>
   <li>Comunicati:
	<ul>
	<li><a href="comunicato.html">Primo Comunicato A/I</a></li>
	<li><a href="comunicato_ricorso.php">Secondo Comunicato A/I</a></li>
	<li><a href="comunicato_zenmai23.php">Comunicato Zenmai23</a></li>
	</ul></li>
<li><a href="link.php">Link su Trenitalia</a></li>
  </ul>

  <hr/>

  <ul>
   <li><a href="protesta.php">Cosa posso fare?</a></li>
	<ol>
	<li><a href="protesta.php#mail">Manda una mail a Trenitalia</a></li>
	<li><a href="protesta.php#banner">Pubblica il banner sul tuo sito</a></li>
	<li>Firma il <a href="http://www.autistici.org/ai/trenitalia/guest/guestbook.php">Guestbook</a></li>
	<li><a href="protesta.php#signature">Usa questa signature!</a></li>
	<li><a href="protesta.php#stampa">Stampa e diffondi il materiale</a></li>
	<li>Fai una <a href="http://autistici.org/it/donate.html">donazione!</a></li>
	</ol>
  </ul>

  <hr/>

  <a href="http://www.inventati.org">Home Page del progetto Autistici/Inventati</a><br/>

 </div>


 <div id="main">


<h2>Mirrors</h2>

<p>
Incredibile. Internet &egrave; proprio un luogo pieno di insidie.<br>
Abbiamo scoperto che il contenuto orribilmente offensivo si trova
 nei luoghi pi&ugrave; disparati, aldil&agrave;
 del nostro controllo. Ce ne scusiamo immensamente e non sappiamo proprio come fare a fermare questo fenomeno fungino!
 </p>



<ul>

<li><a href="http://riseup.net/zenmai23/trenitalia/">http://riseup.net/zenmai23/trenitalia/</a></li>
<li><a href="http://threespeed.org/zenmai23/trenitalia/index.htm">http://threespeed.org/zenmai23/trenitalia/</a></li>
<li><a href="http://www.sindominio.net/~seajob/zenmai23/trenitalia/">http://www.sindominio.net/~seajob/zenmai23/trenitalia/</a></li>
<li><a href="http://italy.indymedia.org/zenmai23/trenitalia/">http://italy.indymedia.org/zenmai23/trenitalia/</a></li>
<!--<li><a href="http://zenmai.kicks-ass.org/">http://zenmai.kicks-ass.org/</a></li>-->
<li><a href="http://zenmai23.linefeed.org/trenitalia/">http://zenmai23.linefeed.org/trenitalia/</a></li>
<li><a href="http://scii.nl/~zenmai23/trenitalia/">http://scii.nl/~zenmai23/trenitalia/</a></li>
<li><a href="http://tazebao.dyne.org/zenmai23/trenitalia/">http://tazebao.dyne.org/zenmai23/trenitalia/</a></li>
<li><a href="http://zenmai23.altervista.org/trenitalia/">http://zenmai23.altervista.org/trenitalia/</a></li>
<li><a href="http://tuxic.nl/mirrors/zenmai23/trenitalia/">http://tuxic.nl/mirrors/zenmai23/trenitalia/</a></li>
<li><a href="http://www.transhack.org/zenmai23/trenitalia/">http://www.transhack.org/zenmai23/trenitalia/</a></li>
<li><a href="http://znetit.mahost.org/trenitalia/">http://znetit.mahost.org/trenitalia/</a></li>
<li><a href="http://membres.lycos.fr/zenmai23/trenitalia/">http://membres.lycos.fr/zenmai23/trenitalia/</a></li>
<li><a href="http://zenmai23.latinowebs.com/">http://zenmai23.latinowebs.com/</a></li>
<li><a href="http://sindominio.net/trenitalia/">http://sindominio.net/trenitalia/</a></li>

</ul>


  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Pagina modificata il 
    13/01/2007   </em></small>
  </p>


 </div>

</body>
</html>
