Autistici.org Website - Public Repository
=====================

This repository is the public mirror of the source code for the [Autistici/Inventati](autistici.org)
website.

You can support the Autistici/Inventati project by updating, translating, or contributing
new guides for the website.


## Contributing with Editing, Updates, Translations

You can contribute to the content of the website by forking this repository and then submitting a merge request.

All content are located in the `src` subdirectory. Pages are
encoded in Markdown, with a small header providing page metadata
(mostly the page title).

The header is YAML-encoded, and it must be separated from the page
content with a line containing only four dashes (`----`).  For
example:

    title: About
    ----
    Contents of the about page.

### Translations

Autistici/Inventati's website is currently fully or partly available in the following languages:

- Italian (the project's native language)
- English (a language most of the collective members are fluent with)
- Spanish (many of us speak at least some)
- French (some of us speak it)
- Portuguese (some of us speak it)
- German (few of us speak it)
- Catalan (none of us speak it, but some very friendly volunteers decided to translate most of the website and we loved the idea)

If you would like to update or add translations in the above languages, we would be very thankful about it,
and would be happy to add them to the website.

If you are thinking of translating into languages other than those, please consider that this
could create false expectations in people, who might think that we would then be able to support
them with technical issues in those languages. If you think it still would be a good idea to have
the whole website translated in your language, then feel free to go ahead, but in that case, consider translating 
as many pages as possible before you submit your project to us.

When adding a translation, create a file with the same name as the original, but with the appropriate language code.
For example, if you wanted to translate the index page into Arabic, you would create a new file in the `src` folder
called `index.ar.md`.

When you're done with your translations, create a merge request and you're done.

Thanks a lot for contributing to the Autistici/Inventati website!


## Making Changes Beyond Content

When editing the website sources, it is a good idea to test the
results locally, before submitting the changes. In order to do so,
there are some setup steps you will need to perform on your system.

## Requirements

To build the website, a few tools will need to be installed:
[gostatic](https://github.com/piranha/gostatic) to generate the HTML
pages, and [sitesearch](https://git.autistici.org/ai/sitesearch) to
generate the search index. The scripts will automatically download and
install these tools if necessary, but you will need a working
[Go](https://golang.org/) development environment.  Furthermore,
testing the website requires running a local Apache server.

On a Debian system, you can install all the requirements with:

    $ sudo apt-get install rsync golang-go

## Testing changes for correctness

There are a few static checks that you can run after modifying the
website. They will check for broken internal links, bad syntax, and
other things. You can invoke the lint script that runs these checks
with:

    $ ./scripts/lint.sh

If this scripts does not output anything, the tests have passed
successfully.

It is a good idea to set up the linter script as a git *pre-commit*
hook, to make it impossible to commit a change that does not pass
these tests successfully:

    $ cat >.git/hooks/pre-commit <<EOF
    #!/bin/sh
    exec ./scripts/lint.sh
    EOF
    $ chmod +x .git/hooks/pre-commit

## How to build the website

Simply run, from the top-level directory:

    $ ./scripts/update.sh

The resulting HTML pages will be found in the `public` directory.

## How to run a test webserver

### Docker

The preferred way, which avoids installing an entire Debian
distribution, would be to use Docker. Build the image with:

    $ docker build -t ai-website .

and run it with:

    $ docker run -p 8080 --network host ai-website

it should then be visible at http://localhost:8080/.

### debootstrap

To check the results of your edits, it is useful to start a local
webserver to inspect the generated HTML pages. In general, you
might want to avoid installing the Apache packages on your local
system (especially on Debian where they automatically start
daemons), so we have prepared a way to install Apache in a
chroot. Run the following commands just once to set it up:

    $ sudo apt-get install debootstrap
    $ sudo ./scripts/install-apache.sh

Then, to start Apache and the search daemon, run (it may ask
you for your password due to the use of *sudo*):

    $ sudo ./scripts/run-test-server.sh

This will start a full server stack (web server and search server)
that you can access by browsing to

    http://localhost:3300

To stop the Apache process when you're done, run:

    $ sudo ./scripts/run-test-server.sh stop


## Updating the FAQ

FAQ pages are currently stored in an old format (compatible with
the *makefaq* tool). Just update them regularly and `update.sh`
will handle them correctly.

## Embedding external data sources

It is possible to use dynamically-generated data in the templates by
placing a script in the `data.d/` directory. The script must be
executable, and its name should not contain dots. Its output must
be valid JSON.

Data from these scripts will be collected in a single dictionary
in the file `data.json`: the data returned by each script will be
present with a key named as the script.

Files ending in `.md_in` will be preprocessed by the template engine
(using the Go html/template syntax) before being rendered into HTML.

As an example of this technique, you can check out:

* `data.d/dns`
* `src/docs/web/domains.en.md_in`



