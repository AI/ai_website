title: Política
----

Política
======

Para ser hospedado em nossos servidores, você tem que compartilhar nossos
princípios de **antifascismo**, **antirracismo**, **antissexismo**,
**anti-homofobia**, **antitransfobia** e **antimilitarismo**. Seus projetos
também devem ser baseados na mesma natureza **não comercial** que mantém nosso
projeto vivo e no desejo de compartilhar e experimentar relações e lutas, com
toda a paciência que isso exige ;))))

Quaisquer serviços fornecidos em nossos servidores não podem ser destinados
(diretamente ou indiretamente) a atividades comerciais ou religiosas, nem a
partidos políticos: para encurtar uma longa história, nós não hospedamos ninguém
que já tenha meios e recursos para espalhar amplamente seus próprios conteúdos
ou ideias, ou quem usa o conceito de delegação (explícito ou implícito) e
representação em suas relações e projetos diários.

O servidor mantém apenas os logs que são estritamente necessários para operações
de depuração e eles não guardam conta de conexão a dados pessoais em nossos
servidores às reais identidades.

Fazer e Não Fazer para serviços específicos:
------------------------------

- [E-Mail (assim como listas de discussão e boletins informativos)](#mail)
- [Ospedagem web e blogs](#web)

------------------------------------------------------------------------------------------------------------------

### <a name="mail"></a>fazer e não fazer (assim como listas de discussão e
boletins informativos para questões relacionadas a spam)

Para abrir uma conta em nosso servidor significa também compartilhar de nosso
[manifesto](/who/manifesto) e tomar nota do seguinte:

- **Limites de espaço em disco**: O uso de sua conta é limitado pelo respeito devido
  a todos que estão usando nossos serviços. Recursos são escassos, o serviço é
  livre e os usuários são muitos. Por este motivo, nós convidamos você a limitar
  o seu uso de nosso disco baixando ou excluindo seus e-mails com a maior
  frequência possível.
- **Uso/Não uso deste serviço**: Caixas de correio que são deixadas sem uso por
  mais de 365 dias serão desabilitadas. Se você acha que não vai usar sua conta
  por mais de 12 meses, mas gostaria de mantê-la ativa mesmo assim, escreva para
  nós.
- **Uso de webmail**: O WebMail precisa de muitos recursos do servidor e,
  portanto, deve ser usado para emergências; para uso normal, convidamos você a
  usar um cliente de e-mail e baixar seus e-mails para seu PC.
- **Senha**: Por motivos relacionados a privacidade, nós nunca mantemos
  registros de ninguém que requisita a ativa de uma conta de e-mail. Para usar
  seu e-mail, você precisará escolher uma pergunta simples que lhe permitirá
  recuperar sua senha no caso de você perdê-la/esquecê-la. Se você também
  esquecer a pergunta ou a resposta da pergunta, não seremos capazes de lhe
  reenviar seus dados para acesso a sua conta novamente. A única coisa que
  seremos capazes de fazer é criar uma nova conta de e-mail para você, mas não
  recuperaremos suas coisas antigas. Então, lembre-se de definir a “pergunta de
  recuperação” em sua conta clicando no link “recover question” que você
  encontra no painel de seu usuário. Lembre-se de periodicamente alterar a sua
  senha: você pode fazê-lo no link “change password” no painel de usuário.
- **Recebendo spam e vírus**: O servidor usa ferramentas de antivírus e
  antispam, mas assim como qualquer outro hospedeiro comercial, eles são pouco
  efetivos.  Todo e-mail recebe um rank de spam usando algumas diretrizes
  difundidas sobre spamming. Se o rank do e-mail for alto suficiente, ele será
  marcado com a palavra \*\*\* SPAM \*\*\* na linha de Assunto do e-mail. Então,
  você será capaz de configurar seu cliente de e-mail para filtra qualquer
  mensagem de spam. Nós recomendamos verificar os e-mails são “marcados” como
  spam, já que o filtro anti-spam é tudo, menos perfeito.
- **Enviando spam e vírus**: Para evitar ter nossos servidores incluídos em
  listas negras em metade do mundo conhecido, nós não queremos que nossas contas
  de e-mails sejam usadas para enviar spam. Qualquer conta apanhada fazendo isso
  será desabilitada sem qualquer aviso.
- **Responsabilidades legais**: Assim como para qualquer outra coisa que você
  faça, esteja ciente de que nossos servidores não são responsáveis pelo que
  você escreve, nem pela guarda de sua própria privacidade. Portanto, convidamos
  você a fazer o máximo com as ferramentas existentes para defender seus
  direitos a privacidade.

--------------------------------------------------------------------------------

### <a name="web"></a>Hospedagem de sites e blogs

- A responsabilidade pelo conteúdo dos sites reside nos webmasters dos sites. O
  coletivo que gere o servidor Autistici-Inventati não pode ser responsabilizado
  pelos conteúdos de qualquer dos sites hospedados. Nós, portanto, convidamos
  todos os webmasters a respeitar nossos princípios e a solicitar espaço web
  apenas após ter lido e concordado com nosso manifesto. O espaço web será
  fornecido apenas após a aprovação coletiva. Sites pessoais não serão
  hospedados a menos que eles publiquem documentos de interesse em particular
  conforme o [manifesto](/who/manifesto "manifesto") e as opiniões do coletivo.
- Lembramos você de não enviar ao site materiais protegidos por copyright (como
  alguns mp3 e divx) que poderiam pôr em perigo a existência de nossos próprios
  servidores (e todos os serviços conectados a eles).
- Quando você trabalhar em seu site, leve em consideração o esforço pelo livre
  compartilhamento de conhecimento pelo qual nós estamos lutando. Nós esperamos
  que você seja sensibilizado por esses esforços. Pense em nós quando você vir o
  pequeno porém significante aviso de “copyright” na parte inferior de sua
  página. O material publicado neste servidor será lançado pelo menos sob uma
  **licença livre** (como, por exemplo, a [GPL](http://www.gnu.org) ou a
  [Creative Commons](http://www.creativecommons.it))se você decidir usar alguma
  licença com copyright.
- Os limites do seu espaço web são limitados pelos recursos da máquina. Isso
  significa que não é bom usar espaço para dados pessoais ou para arquivos
  grandes, a menos que seja estritamente necessário. **Geralmente, nós confiamos
  na colaboração e responsabilidade**. Caso você tenha **arquivos grandes** para
  manter online, nós convidamos você a nos contatar para verificar a
  disponibilidade do espaço em disco antes de enviá-los.
- Nossos servidores significam a salvaguarda da privacidade e anonimato de seus
  usuários: portanto, não é permitido usar contadores ou outros serviços de
  estatística web (como shinycat ou google analytics) em qualquer página de
  site, já que eles registram os IPs de pessoas visitando o site, o que é
  completamente incompatível com nossos principais princípios. Se você realmente
  deseja saber quantas pessoas visitam seu site, sinta-se à vontade para usar
  [nosso serviço](/services/piwik "Statistiche Piwik A/I") de estatísticas web.
- Doxing (i.e. a publicação de dados pessoais de outras pessoas) não é permitido
  porque nós não temos como estabelecer a boa-fé com a qual essa informação foi
  publicada ou as consequências para as pessoas que são sujeitas ao doxing e
  também porque isso nos expõe a riscos legais muito altos. Nós decidimos banir
  doxing também considerando que para esse tipo de atividade há muitos serviços
- Quando você trabalhar em seu site, pense no fato de que nem todo mundo possui
  acesso às últimas tecnologias existentes e que há também pessoas que não podem
  ver; para nós, a **acessibilidade** dos sites hospedados neste servidor é
  sempre importante.
- Na [página de propaganda](/who/propaganda "propaganda") de nosso site você
  encontrará uma pequena logo do projeto autistici.org/inventati.org. Nós
  ficaríamos muito felizes de saber que os sites hospedados em nossos servidores
  o exibem em alguma lugar em suas páginas ;))))). Além disso, no caso de você
  estar usando seu próprio domínio (leia as [notas técnicas](/docs/web/domains
  "Registra un dominio")), a inclusão de uma pequena logo do Inventati/Autistici
  de nossa página é considerado um pequeno, mas importante sinal de que você
  compartilha e apoia nosso projeto.
- **Não é permitido usar o espaço designado como um redirecionamento para outros
  sites**, pois nós consideramos isso um gasto desnecessário de nossos recursos.
- Espaço do servidor deixar sem uso por mais de 90 dias a partir da ativação
  será desabilitado e excluído (pelos mesmos motivos acima).
- Se o uso de mysql tiver sido solicitada, a manutenção de nosso próprio banco
  de dados ocorre por meio de interface web. NÃO É NECESSÁRIO instalar qualquer
  software como phpadmin ou interfaces web similares para gerenciar sua própria
  conta MySQL, porque nós já instalamos e mantemos eles com cuidado.

- - -

Se você tiver mais dúvidas, dê uma olhada em nosso [FAQ](/docs/faq/) ou [nos contate](mailto:info@autistici.org)!
