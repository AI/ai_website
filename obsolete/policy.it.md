title: Policy
----

Policy 
======

Le pregiudiziali per poter partecipare ai servizi offerti su questo server sono
la condivisione dei principi di **antifascismo, antirazzismo, antisessismo,
antiomofobia, antitransfobia, antimilitarismo e non commercialità** che animano
questo progetto, oltre ovviamente a una buona dose di volontà di condivisione e
di relazione e a un bel po' di pazienza ;)))))

Spazi e servizi di questo server non vengono destinati ad attività (direttamente
o indirettamente) commerciali, al clero, ai partiti politici istituzionali o
comunque, in sintesi, a qualunque realtà che disponga di altri potenti mezzi per
veicolare i propri contenuti, o che utilizzi il concetto di delega (esplicita o
implicita) per la gestione di rapporti e progetti.

Il server conserva solo i log strettamente necessari a operazioni di debugging,
che comunque non sono associabili in alcun modo ai dati identificativi degli
utenti dei nostri servizi. Per saperne di piu' su come gestiamo i tuoi dati,
puoi leggere [questo documento](/who/your_data)

Policy specifiche dei servizi:
------------------------------

- [E-Mail (e anche mailing list e newsletter)](#mail)
- [Siti web e blog](#web)

------------------------------------------------------------------------------------------------------------------

### <a name="mail"></a>E-Mail (e anche mailing list e newsletter per quanto riguarda lo spam)

Aprire una casella sul nostro server significa anche condividere il nostro
[manifesto](/who/manifesto) e prendere atto di quanto segue.

- **Limiti di spazio su disco**: L'utilizzo della tua casella è limitato dal
  rispetto di tutti coloro che devono usare i nostri servizi.  Le risorse
  scarseggiano, il servizio è gratuito e gli utenti sono molti. Ti invitiamo
  quindi a utilizzare con moderazione lo spazio su disco, scaricando o
  cancellando la tua posta il più spesso possibile.
- **Utilizzo/Inutilizzo del servizio**: Le caselle non usate per più di 365
  giorni verranno cancellate. Se pensi di non utilizzare la casella per più di
  un anno ma intendi comunque mantenerla,
  [scrivici](mailto:info@autistici.org).
- **Uso della WebMail**: La webmail impegna molte risorse del server, ed è
  quindi da utilizzare esclusivamente per le emergenze; normalmente utilizza un
  programma di posta (come Thunderbird) e scarica la tua mail su un computer.
- **Password:** Per ragioni legate alla privacy, noi non manteniamo nessun
  registro di chi richiede l'attivazione di una casella di posta.  Per poter
  usare la tua email dovrai scegliere una semplice domanda che ti consentirà il
  recupero della password in caso di smarrimento (la "domanda del gatto"). Se
  dimentichi anche la domanda o la risposta a quest'ultima, noi non potremo
  rispedirti i dati per accedere alla casella, ma solo creartene una nuova.
  Ricorda quindi di impostare il sistema automatico per recuperarla: per farlo
  ti bastera' cliccare su "imposta il gatto" dal tuo pannello utente. Un'altra
  cosa fondamentale per la tua e per la nostra sicurezza è cambiare
  periodicamente la tua password: per farlo clicca su "cambia password" dal tuo
  pannello utente.
- **Ricevere Spam e Virus**: Il server utilizza sistemi di antivirus e antispam,
  ma come per i servizi commerciali, funzionano poco. A ogni mail viene dato un
  punteggio. Se la mail raggiunge il punteggio stabilito, nel Subject viene
  aggiunta la dizione \*\*\* SPAM \*\*\*. Puoi quindi configurare il tuo client
  per filtrare i messaggi di spam. Consigliamo comunque di controllare le mail
  "taggate" (contrassegnate) come spam, visto che il filtro non è infallibile.
- **Mandare Spam e Virus**: Per evitare di essere inseriti nella blacklist di
  mezzo mondo, non vogliamo che i nostri account di posta vengano utilizzati per
  spam. Se ci accorgiamo che una casella di posta viene usata per inviare spam
  la disabiliteremo senza preavviso.
- **Responsabilità legale**: Come per tutto quello che fai, è importante che tu
  sappia che il nostro server non è responsabile per quello che scrivi, né
  tantomeno per la salvaguardia della tua privacy. Ti invitiamo quindi a
  utilizzare al meglio tutti gli [strumenti](/docs/mail/privacymail "proteggi la
  tua privacy") che esistono per difendere il tuo diritto alla privacy.

---------------------------------------------------------------------------------------------------------------

### <a name="web"></a>Siti web e blog

- La responsabilità per il contenuto del sito è del webmaster del sito stesso.
  Il collettivo di gestione del server Autistici-Inventati non si assume alcuna
  responsabilità in merito al contenuto dei siti ospitati.  Invitiamo quindi
  tutti i webmaster a rispettare i nostri principi e a richiedere lo spazio solo
  una volta condiviso e sottoscritto il nostro [manifesto](/who/manifesto
  "manifesto").
- Lo spazio web viene concesso a esclusivo giudizio del collettivo. Non vengono
  concessi spazi a uso personale, a meno che in esso non vengano pubblicati
  materiali di particolare interesse secondo i principi del nostro
  [manifesto](/who/manifesto "manifesto") e l'opinione del collettivo.
- Ricordiamo di non uplodare sul sito materiali protetti da copyright (come
  talvolta sono mp3 e divx), che metterebbero a repentaglio l'esistenza stessa
  del server (e di tutti i servizi a esso connessi).
- Quando lavorate sul vostro sito, pensate alle battaglie sulla libera
  circolazione dei saperi che stiamo conducendo; speriamo che siate sensibili a
  queste lotte; pensateci quindi quando scrivete il piccolo ma significante
  "copyright" in fondo alla pagina. Il materiale pubblicato sui server A/I
  dovrà quindi essere rilasciato (qualora si decida di usare una licenza di
  copyright), quantomeno, con una **licenza libera** (per citarne alcune
  possibili: [GPL](http://www.gnu.org) e [Creative
  Commons](http://www.creativecommons.it)).
- Il limite del tuo spazio web è limitato dalle risorse delle nostre macchine.
  Questo vuol dire che non è bene utilizzare lo spazio per dati personali e di
  grosse dimensioni, a meno che non sia strettamente necessario. **In generale,
  confidiamo nella collaborazione e nel senso di responsabilità dei nostri
  utenti**. Se hai **file molto grossi** da tenere online, ti invitiamo a
  contattarci per verificare la disponibilità dello spazio disco prima
  dell'upload.
- A/I intende tutelare la privacy e l'anonimato dei propri utenti: non è quindi
  permesso inserire nelle proprie pagine contatori o altri servizi (tipo
  shinystat o google analytics) che loggano l'IP di chi visita il sito; se
  proprio desiderate un contatore, usate [piwik](/services/piwik "Statistiche
  Piwik A/I"), il nostro nuovo servizio di statistiche.
- Non è permesso il doxing (cioè la pubblicazione di dati personali altrui)
  perché non siamo in grado di valutarne né la buona fede, né le conseguenze per
  le persone che lo subiscono e perché ci espone a rischi legali eccessivi,
  anche considerato che per fare doxing non avete bisogno dei nostri servizi e
  potete trovarne tanti altri (meglio se hidden service).
- Quando lavorate al vostro sito, pensate al fatto che non tutt<span
  class="red">\*</span> posseggono tecnologie di ultima generazione e che anche
  persone non vedenti potrebbero volerlo consultare; per noi **l'accessibilità**
  dei siti ospitati su questo server è importante (vedi
  <http://www.ecn.org/xs2web> per maggiori informazioni sul tema
  dell'accessibilità).
- Tra i [materiali](/who/propaganda "propaganda") è disponibile un loghino del
  progetto autistici.org/inventati.org e saremmo molto contente se i siti
  ospitati sui nostri server lo inserissero da qualche parte nelle loro home
  page ;))))). Inoltre, nel caso in cui utilizzate un vostro dominio (leggi
  [note tecniche](/docs/web/domains "Registra un dominio")), mettere un piccolo
  logo di Autistici/Inventati sulla vostra home page è ritenuto un piccolo ma
  necessario segno di condivisione del progetto.
- **Non è permesso utilizzare lo spazio assegnato come redirect su altri siti**,
  in quanto lo consideriamo un inutile spreco delle nostre risorse.
- Gli spazi lasciati inutilizzati per più di 90 giorni dall'attivazione verranno
  rimossi (per lo stesso motivo).
- Se avete richiesto l'uso di MySQL, la gestione del vostro database avverrà
  attraverso un'interfaccia web. NON È NECESSARIO, anzi è male, installare
  software come phpadmin o interfacce web simili per l'amministrazione
  dell'account MySQL nel proprio spazio web, poiché le abbiamo già installate e
  predisposte con tanto amore.

- - -

Per ulteriori dubbi, dai un'occhiata alle nostre [faq](/docs/faq/) oppure
[contattaci](mailto:info@autistici.org).


